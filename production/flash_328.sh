#!/bin/bash

# flash optiloader from the current directory onto an atmega328
avrdude -c usbtiny -p m328 -v -e -U efuse:w:0x05:m -U hfuse:w:0xD6:m -U lfuse:w:0xFF:m && avrdude -c usbtiny -p m328 -v -e -U flash:w:optiboot_atmega328.hex -U lock:w:0x0F:m

