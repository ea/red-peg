#include <red_peg.h>
#include <Wire.h>
#include <RTClib.h>
#include <MCP342x.h>
#include <SPI.h>
#include <SdFat.h>

#define SD_CS_PIN 5  //5
SdFat SD;
File myFile;
red_peg RP;
RTC_DS1307 rtc;
uint8_t rtc_address = 0x68;
uint8_t mpc_address = 0x6F;
MCP342x adc = MCP342x(mpc_address);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(BAUD);
  Serial.println(F("start rp_board_test"));
  RP.begin();
  Wire.begin();

  // Reset devices
  MCP342x::generalCallReset();
  delay(1); // MC342x needs 300us to settle, wait 1ms
  
  // Check device present
  Wire.requestFrom(mpc_address, (uint8_t)1);
  Serial.print(F("MCP3428..............."));
  if (!Wire.available()) {
    Serial.print("FAIL ");
    Serial.println(mpc_address, HEX);
  } else {
    Serial.println("OK");
  }
  Serial.print(F("ADC check............."));
  long value = 0;
  MCP342x::Config status;
  // Initiate a conversion; convertAndRead() will wait until it can be read
  uint8_t err = adc.convertAndRead(MCP342x::channel3, MCP342x::oneShot,
           MCP342x::resolution16, MCP342x::gain1,
           1000000, value, status);
  if (err) {
    Serial.print("FAIL ");
    Serial.println(err);
  }
  else {
    float voltage = value * (2.0 / 32575.0);
    Serial.print("OK\t");
    //Serial.print("OK Value: ");
    //Serial.print(value);
    //Serial.print(", ");
    Serial.print(voltage);
    Serial.print("V");
    Serial.println();
  }
  
  // check we have an RTC attached
  Serial.print(F("RTC..................."));
  Wire.requestFrom(rtc_address, (uint8_t)7);
  if (!Wire.available()) {
    Serial.print("FAIL ");
    Serial.print(rtc_address, HEX);
  } else {
    Serial.print("OK");
  }
  if (!rtc.begin()) {
    Serial.println(F("MISSING"));
  } else {
    Serial.print(F("\t"));
    rtc.adjust(DateTime(2000, 01, 02, 03, 04, 05));
    reportTimeNow();
  }

  delay(200);
  Serial.print(F("RPS..................."));
  // check Red-Peg slave operation
  RP.sensorsOn();
  delay(100);
  for (int i=0; i<3; i++) {
    RP.get(MA4_20);
    delay(10);
//    RP.get(EMPTY);
//    delay(10);
  }
  t_SensorData depth = RP.get(MA4_20);
  RP.sensorsOff();
  if (depth.sensor == MA4_20) {
    //Serial.print("Value: ");
    //Serial.print(depth.reading);
    //Serial.print(", ");
    //Serial.print(RP.mA(depth));
    //Serial.print(" mA, ");
    //Serial.print(RP.level(depth, 7000));
    //Serial.print(" mm");
    Serial.println("OK");
  } else {
    Serial.print("FAIL: ");
    RP.print_data(depth);
  }

  Serial.print("SD present............");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output
  // or the SD library functions will not work.
  pinMode(10, OUTPUT);
  pinMode(SD_CS_PIN, OUTPUT);

  if (!SD.begin(SD_CS_PIN)) {
    Serial.println("FAIL, no card?");
    return;
  }
  Serial.println("OK");

    myFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("SD Write..............");
    myFile.println("testing 1, 2, 3.");
    // close the file:
    myFile.close();
    Serial.println("OK");
  } else {
    // if the file didn't open, print an error:
    Serial.println("FAIL");
  }

  // re-open the file for reading:
  myFile = SD.open("test.txt");
  if (myFile) {
    Serial.print("SD read...............");

    // read from the file until there's nothing else in it:
    Serial.println("OK");
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
    // then remove it
    SD.remove("test.txt");
  } else {
    // if the file didn't open, print an error:
    Serial.println("FAIL");
  }

  
}

void loop() {
  // put your main code here, to run repeatedly:

}

void reportTimeNow() {
  DateTime now = rtc.now();
  char buf[5];
  sprintf(buf, "%04d", now.year());
  Serial.print(buf);
  Serial.write('-');
  sprintf(buf, "%02d", now.month());
  Serial.print(buf);
  Serial.write('-');
  sprintf(buf, "%02d", now.day());
  Serial.print(buf);
  Serial.write('T');
  sprintf(buf, "%02d", now.hour());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.minute());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.second());
  Serial.print(buf);
  Serial.write('Z');
  Serial.println();
}
