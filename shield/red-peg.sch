EESchema Schematic File Version 2
LIBS:red-peg-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:red-peg
LIBS:red-peg-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 12750 5200 1000 1450
U 57DA7D52
F0 "Non-electrical parts" 60
F1 "file57DA7D51.sch" 60
$EndSheet
$Comp
L MCP3428 U2
U 1 1 56685029
P 3600 6200
F 0 "U2" H 3750 6450 60  0000 R CNN
F 1 "MCP3428" H 3750 6350 60  0000 R CNN
F 2 "Local_Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3600 6200 60  0001 C CNN
F 3 "" H 3600 6200 60  0000 C CNN
F 4 "+-2V 16-bit ADC" H 0   0   50  0001 C CNN "Description"
F 5 "MCP3428-E/SL-ND" H 0   0   50  0001 C CNN "digikey"
F 6 "1825014" H 0   0   50  0001 C CNN "farnell"
F 7 "MCP3428" H 0   0   50  0001 C CNN "internal p/n"
F 8 "MCP3428-E/SL" H 3600 6200 60  0001 C CNN "manf#"
F 9 "MCP3428-E/SL" H 0   0   50  0001 C CNN "part number"
F 10 "703-7945" H 0   0   50  0001 C CNN "rs"
	1    3600 6200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5668598D
P 3450 7150
F 0 "#PWR01" H 3450 6900 50  0001 C CNN
F 1 "GND" H 3450 7000 50  0000 C CNN
F 2 "" H 3450 7150 50  0000 C CNN
F 3 "" H 3450 7150 50  0000 C CNN
	1    3450 7150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 56685BD5
P 3350 5450
F 0 "#PWR02" H 3350 5300 50  0001 C CNN
F 1 "+5V" H 3350 5590 50  0000 C CNN
F 2 "" H 3350 5450 50  0000 C CNN
F 3 "" H 3350 5450 50  0000 C CNN
	1    3350 5450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 56686099
P 4200 7550
F 0 "#PWR03" H 4200 7300 50  0001 C CNN
F 1 "GND" H 4200 7400 50  0000 C CNN
F 2 "" H 4200 7550 50  0000 C CNN
F 3 "" H 4200 7550 50  0000 C CNN
	1    4200 7550
	1    0    0    -1  
$EndComp
$Comp
L ARDUINO_UNO SHIELD1
U 1 1 56686840
P 2050 2450
F 0 "SHIELD1" H 1700 3400 60  0000 C CNN
F 1 "ARDUINO_UNO" H 2100 1500 60  0000 C CNN
F 2 "arduino_shields:ARDUINO_SHIELD" H 2050 2450 60  0001 C CNN
F 3 "" H 2050 2450 60  0000 C CNN
F 4 "red-peg-r1" H 2050 2450 60  0001 C CNN "manf#"
F 5 "red-peg-r1" H 0   0   50  0001 C CNN "part number"
	1    2050 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 56686C84
P 3750 1850
F 0 "#PWR04" H 3750 1600 50  0001 C CNN
F 1 "GND" H 3750 1700 50  0000 C CNN
F 2 "" H 3750 1850 50  0000 C CNN
F 3 "" H 3750 1850 50  0000 C CNN
	1    3750 1850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 56686CF7
P 850 1700
F 0 "#PWR05" H 850 1550 50  0001 C CNN
F 1 "+5V" H 850 1840 50  0000 C CNN
F 2 "" H 850 1700 50  0000 C CNN
F 3 "" H 850 1700 50  0000 C CNN
	1    850  1700
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 56686D1F
P 950 1950
F 0 "#PWR06" H 950 1800 50  0001 C CNN
F 1 "+3.3V" H 950 2090 50  0000 C CNN
F 2 "" H 950 1950 50  0000 C CNN
F 3 "" H 950 1950 50  0000 C CNN
	1    950  1950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 56686F71
P 900 2650
F 0 "#PWR07" H 900 2400 50  0001 C CNN
F 1 "GND" H 900 2500 50  0000 C CNN
F 2 "" H 900 2650 50  0000 C CNN
F 3 "" H 900 2650 50  0000 C CNN
	1    900  2650
	1    0    0    -1  
$EndComp
$Comp
L DS1307_RTC U5
U 1 1 56687148
P 9050 5500
F 0 "U5" H 9200 5750 60  0000 R CNN
F 1 "DS1307_RTC" H 9200 5650 60  0000 R CNN
F 2 "Local_Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 9050 5500 60  0001 C CNN
F 3 "" H 9050 5500 60  0000 C CNN
F 4 "DS1307 RTC" H 0   0   50  0001 C CNN "Description"
F 5 "RS" H 0   0   50  0001 C CNN "Source"
F 6 "DS1307ZN+-ND" H 0   0   50  0001 C CNN "digikey"
F 7 "DS1307" H 0   0   50  0001 C CNN "internal p/n"
F 8 "DS1307ZN+" H 9050 5500 60  0001 C CNN "manf#"
F 9 "DS1307ZN+" H 0   0   50  0001 C CNN "part number"
F 10 "732-7333" H 0   0   50  0001 C CNN "rs"
	1    9050 5500
	1    0    0    -1  
$EndComp
$Comp
L Battery-RESCUE-red-peg BT1
U 1 1 566871DD
P 10400 4950
F 0 "BT1" H 10500 5000 50  0000 L CNN
F 1 "CH291-1220LF" H 10500 4900 50  0000 L CNN
F 2 "Connect:CR1220" V 10400 4990 50  0001 C CNN
F 3 "" V 10400 4990 50  0000 C CNN
F 4 "~" H 10400 4950 60  0001 C CNN "Characteristics"
F 5 "~" H 10400 4950 60  0001 C CNN "Critical"
F 6 "~" H 10400 4950 60  0001 C CNN "Description"
F 7 "~" H 10400 4950 60  0001 C CNN "MFN"
F 8 "~" H 10400 4950 60  0001 C CNN "MFP"
F 9 "~" H 10400 4950 60  0001 C CNN "Notes"
F 10 "~" H 10400 4950 60  0001 C CNN "Package ID"
F 11 "~" H 10400 4950 60  0001 C CNN "Source"
F 12 "2064722" H 10400 4950 60  0001 C CNN "farnell"
F 13 "CH291-1220LF" H 10400 4950 60  0001 C CNN "manf#"
F 14 "CH291-1220LF" H 0   0   50  0001 C CNN "part number"
F 15 "~" H 10400 4950 60  0001 C CNN "supplier1_link"
F 16 "~" H 10400 4950 60  0001 C CNN "supplier2_link"
F 17 "~" H 10400 4950 60  0001 C CNN "supplier2_name"
	1    10400 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 566872E4
P 10400 5100
F 0 "#PWR08" H 10400 4850 50  0001 C CNN
F 1 "GND" H 10400 4950 50  0000 C CNN
F 2 "" H 10400 5100 50  0000 C CNN
F 3 "" H 10400 5100 50  0000 C CNN
	1    10400 5100
	1    0    0    -1  
$EndComp
$Comp
L Crystal_Small Y1
U 1 1 56687334
P 10550 5650
F 0 "Y1" H 10550 5750 50  0000 C CNN
F 1 "32.768kHz" H 10550 5550 50  0000 C CNN
F 2 "local:Crystal_SMD_ABS25" H 10550 5650 50  0001 C CNN
F 3 "" H 10550 5650 50  0000 C CNN
F 4 "32.768kHz resonator" H 0   0   50  0001 C CNN "Description"
F 5 "1611824" H 0   0   50  0001 C CNN "farnell"
F 6 "X32K" H 0   0   50  0001 C CNN "internal p/n"
F 7 "ABS25-32.768KHZ-T" H 10550 5650 60  0001 C CNN "manf#"
F 8 "ABS25-32.768KHZ-T" H 0   0   50  0001 C CNN "part number"
	1    10550 5650
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR09
U 1 1 5668749F
P 9650 4700
F 0 "#PWR09" H 9650 4550 50  0001 C CNN
F 1 "+5V" H 9650 4840 50  0000 C CNN
F 2 "" H 9650 4700 50  0000 C CNN
F 3 "" H 9650 4700 50  0000 C CNN
	1    9650 4700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 56687606
P 9650 6350
F 0 "#PWR010" H 9650 6100 50  0001 C CNN
F 1 "GND" H 9650 6200 50  0000 C CNN
F 2 "" H 9650 6350 50  0000 C CNN
F 3 "" H 9650 6350 50  0000 C CNN
	1    9650 6350
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 566879EB
P 3150 1200
F 0 "R6" V 3230 1200 50  0000 C CNN
F 1 "2.2k" V 3150 1200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3080 1200 50  0001 C CNN
F 3 "" H 3150 1200 50  0000 C CNN
F 4 "2.2k 0603" H 0   0   50  0001 C CNN "Description"
F 5 "2447320" H 0   0   50  0001 C CNN "farnell"
F 6 "R6R2200" H 0   0   50  0001 C CNN "internal p/n"
F 7 "MCWR06X2201FTL" V 3150 1200 60  0001 C CNN "manf#"
F 8 "MCWR06X2201FTL" H 0   0   50  0001 C CNN "part number"
	1    3150 1200
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 56687B3B
P 3350 1200
F 0 "R7" V 3430 1200 50  0000 C CNN
F 1 "2.2k" V 3350 1200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3280 1200 50  0001 C CNN
F 3 "" H 3350 1200 50  0000 C CNN
F 4 "2.2k 0603" H 0   0   50  0001 C CNN "Description"
F 5 "2447320" H 0   0   50  0001 C CNN "farnell"
F 6 "R6R2200" H 0   0   50  0001 C CNN "internal p/n"
F 7 "MCWR06X2201FTL" V 3350 1200 60  0001 C CNN "manf#"
F 8 "MCWR06X2201FTL" H 0   0   50  0001 C CNN "part number"
	1    3350 1200
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR011
U 1 1 56687B73
P 3250 900
F 0 "#PWR011" H 3250 750 50  0001 C CNN
F 1 "+5V" H 3250 1040 50  0000 C CNN
F 2 "" H 3250 900 50  0000 C CNN
F 3 "" H 3250 900 50  0000 C CNN
	1    3250 900 
	1    0    0    -1  
$EndComp
$Comp
L microSD_card U6
U 1 1 566884D4
P 10250 1400
F 0 "U6" H 10450 1550 60  0000 L CNN
F 1 "47309-2651 uSDcard" V 10850 500 60  0000 L CNN
F 2 "Connect:Wafer_Horizontal22.5x5.8x7RM2.5-8" H 10250 1400 60  0001 C CNN
F 3 "" H 10250 1400 60  0000 C CNN
F 4 "Micro-SD header" H 0   0   50  0001 C CNN "Description"
F 5 "1568092" H 0   0   50  0001 C CNN "farnell"
F 6 "SD1" H 0   0   50  0001 C CNN "internal p/n"
F 7 "47309-2651" H 10250 1400 60  0001 C CNN "manf#"
F 8 "47309-2651" H 0   0   50  0001 C CNN "part number"
	1    10250 1400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR012
U 1 1 56688A76
P 10200 1200
F 0 "#PWR012" H 10200 1050 50  0001 C CNN
F 1 "+3.3V" H 10200 1340 50  0000 C CNN
F 2 "" H 10200 1200 50  0000 C CNN
F 3 "" H 10200 1200 50  0000 C CNN
	1    10200 1200
	1    0    0    -1  
$EndComp
$Comp
L 74HC125 U4
U 1 1 56688B18
P 8850 1500
F 0 "U4" H 9000 1750 60  0000 R CNN
F 1 "74HC125" H 9000 1650 60  0000 R CNN
F 2 "Local_Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 8850 1500 60  0001 C CNN
F 3 "" H 8850 1500 60  0000 C CNN
F 4 "~" H 8850 1500 60  0001 C CNN "Characteristics"
F 5 "~" H 8850 1500 60  0001 C CNN "Critical"
F 6 "~" H 8850 1500 60  0001 C CNN "Description"
F 7 "~" H 8850 1500 60  0001 C CNN "MFN"
F 8 "~" H 8850 1500 60  0001 C CNN "MFP"
F 9 "~" H 8850 1500 60  0001 C CNN "Notes"
F 10 "~" H 8850 1500 60  0001 C CNN "Package ID"
F 11 "~" H 8850 1500 60  0001 C CNN "Source"
F 12 "2342357" H 0   0   50  0001 C CNN "farnell"
F 13 "74HC125" H 0   0   50  0001 C CNN "internal p/n"
F 14 "CD74HC125M96" H 8850 1500 60  0001 C CNN "manf#"
F 15 "CD74HC125M96" H 0   0   50  0001 C CNN "part number"
F 16 "~" H 8850 1500 60  0001 C CNN "supplier1_link"
F 17 "~" H 8850 1500 60  0001 C CNN "supplier1_name"
F 18 "~" H 8850 1500 60  0001 C CNN "supplier2_link"
F 19 "~" H 8850 1500 60  0001 C CNN "supplier2_name"
	1    8850 1500
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR013
U 1 1 56689527
P 9450 850
F 0 "#PWR013" H 9450 700 50  0001 C CNN
F 1 "+3.3V" H 9450 990 50  0000 C CNN
F 2 "" H 9450 850 50  0000 C CNN
F 3 "" H 9450 850 50  0000 C CNN
	1    9450 850 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 56689B87
P 9450 2950
F 0 "#PWR014" H 9450 2700 50  0001 C CNN
F 1 "GND" H 9450 2800 50  0000 C CNN
F 2 "" H 9450 2950 50  0000 C CNN
F 3 "" H 9450 2950 50  0000 C CNN
	1    9450 2950
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P1
U 1 1 5668B556
P 1000 6200
F 0 "P1" H 1000 6450 50  0000 C CNN
F 1 "4-20mA" V 1100 6200 50  0000 C CNN
F 2 "Connect:bornier4" H 1000 6200 50  0001 C CNN
F 3 "" H 1000 6200 50  0000 C CNN
F 4 "~" H 1000 6200 60  0001 C CNN "Characteristics"
F 5 "~" H 1000 6200 60  0001 C CNN "Critical"
F 6 "wago 256 endplate" H 1000 6200 60  0001 C CNN "Description"
F 7 "Wago" H 1000 6200 60  0001 C CNN "MFN"
F 8 "~" H 1000 6200 60  0001 C CNN "MFP"
F 9 "~" H 1000 6200 60  0001 C CNN "Notes"
F 10 "~" H 1000 6200 60  0001 C CNN "Package ID"
F 11 "~" H 1000 6200 60  0001 C CNN "Source"
F 12 "4015393" H 1000 6200 60  0001 C CNN "farnell"
F 13 "256-100" H 0   0   50  0001 C CNN "manf#"
F 14 "256-100" H 0   0   50  0001 C CNN "part number"
F 15 "~" H 1000 6200 60  0001 C CNN "supplier1_link"
F 16 "~" H 1000 6200 60  0001 C CNN "supplier1_name"
F 17 "~" H 1000 6200 60  0001 C CNN "supplier2_link"
F 18 "~" H 1000 6200 60  0001 C CNN "supplier2_name"
	1    1000 6200
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR015
U 1 1 5668B762
P 1350 6800
F 0 "#PWR015" H 1350 6550 50  0001 C CNN
F 1 "GND" H 1350 6650 50  0000 C CNN
F 2 "" H 1350 6800 50  0000 C CNN
F 3 "" H 1350 6800 50  0000 C CNN
	1    1350 6800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 5668BF96
P 1200 4600
F 0 "#PWR016" H 1200 4350 50  0001 C CNN
F 1 "GND" H 1200 4450 50  0000 C CNN
F 2 "" H 1200 4600 50  0000 C CNN
F 3 "" H 1200 4600 50  0000 C CNN
	1    1200 4600
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 5668C088
P 950 4400
F 0 "P2" H 950 4600 50  0000 C CNN
F 1 "SDI-12" V 1050 4400 50  0000 C CNN
F 2 "Connect:bornier3" H 950 4400 50  0001 C CNN
F 3 "" H 950 4400 50  0000 C CNN
F 4 "wago 256 endplate" H 0   0   50  0001 C CNN "Description"
F 5 "Wago" H 0   0   50  0001 C CNN "MFN"
F 6 "4015393" H 0   0   50  0001 C CNN "farnell"
F 7 "256-100" H 0   0   50  0001 C CNN "manf#"
F 8 "256-100" H 0   0   50  0001 C CNN "part number"
	1    950  4400
	-1   0    0    1   
$EndComp
$Comp
L VN5160S-E U3
U 1 1 5668C4EF
P 6150 5350
F 0 "U3" H 6500 5850 60  0000 R CNN
F 1 "VN5160S-E" H 6800 4650 60  0000 R CNN
F 2 "Local_Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 6150 5350 60  0001 C CNN
F 3 "" H 6150 5350 60  0000 C CNN
F 4 "High-side power switch" H 0   0   50  0001 C CNN "Description"
F 5 "1739390" H 0   0   50  0001 C CNN "farnell"
F 6 "VN5160S" H 0   0   50  0001 C CNN "internal p/n"
F 7 "VN5160S-E" H 6150 5350 60  0001 C CNN "manf#"
F 8 "VN5160S-E" H 0   0   50  0001 C CNN "part number"
	1    6150 5350
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR017
U 1 1 5668CB4B
P 700 1450
F 0 "#PWR017" H 700 1300 50  0001 C CNN
F 1 "+12V" H 700 1590 50  0000 C CNN
F 2 "" H 700 1450 50  0000 C CNN
F 3 "" H 700 1450 50  0000 C CNN
	1    700  1450
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR018
U 1 1 5668CD62
P 6950 4550
F 0 "#PWR018" H 6950 4400 50  0001 C CNN
F 1 "+12V" H 6950 4690 50  0000 C CNN
F 2 "" H 6950 4550 50  0000 C CNN
F 3 "" H 6950 4550 50  0000 C CNN
	1    6950 4550
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 5668D432
P 6050 5850
F 0 "R8" V 6130 5850 50  0000 C CNN
F 1 "10k" V 6050 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5980 5850 50  0001 C CNN
F 3 "" H 6050 5850 50  0000 C CNN
F 4 "10k 0603" H 0   0   50  0001 C CNN "Description"
F 5 "2447230" H 0   0   50  0001 C CNN "farnell"
F 6 "R6K010" H 0   0   50  0001 C CNN "internal p/n"
F 7 "MCWR06X1002FTL" H 0   0   50  0001 C CNN "manf#"
F 8 "MCWR06X1002FTL" H 0   0   50  0001 C CNN "part number"
	1    6050 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 5668D47D
P 6950 6300
F 0 "#PWR019" H 6950 6050 50  0001 C CNN
F 1 "GND" H 6950 6150 50  0000 C CNN
F 2 "" H 6950 6300 50  0000 C CNN
F 3 "" H 6950 6300 50  0000 C CNN
	1    6950 6300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P3
U 1 1 5668E27A
P 2550 4250
F 0 "P3" H 2550 4450 50  0000 C CNN
F 1 "16BitADC" V 2650 4250 50  0000 C CNN
F 2 "Connect:bornier3" H 2550 4250 50  0001 C CNN
F 3 "" H 2550 4250 50  0000 C CNN
F 4 "wago 256 endplate" H 0   0   50  0001 C CNN "Description"
F 5 "Wago" H 0   0   50  0001 C CNN "MFN"
F 6 "4015393" H 0   0   50  0001 C CNN "farnell"
F 7 "256-100" H 0   0   50  0001 C CNN "manf#"
F 8 "256-100" H 0   0   50  0001 C CNN "part number"
	1    2550 4250
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR020
U 1 1 5668E308
P 3000 4050
F 0 "#PWR020" H 3000 3900 50  0001 C CNN
F 1 "+5V" H 3000 4190 50  0000 C CNN
F 2 "" H 3000 4050 50  0000 C CNN
F 3 "" H 3000 4050 50  0000 C CNN
	1    3000 4050
	1    0    0    -1  
$EndComp
$Comp
L Q_NJFET_GSD Q1
U 1 1 5668E5AF
P 2900 4600
F 0 "Q1" H 3200 4650 50  0000 R CNN
F 1 "BSS138" H 3400 4550 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3100 4700 50  0001 C CNN
F 3 "" H 2900 4600 50  0000 C CNN
F 4 "~" H 2900 4600 60  0001 C CNN "Characteristics"
F 5 "~" H 2900 4600 60  0001 C CNN "Critical"
F 6 "~" H 2900 4600 60  0001 C CNN "Description"
F 7 "~" H 2900 4600 60  0001 C CNN "MFN"
F 8 "~" H 2900 4600 60  0001 C CNN "MFP"
F 9 "~" H 2900 4600 60  0001 C CNN "Notes"
F 10 "~" H 2900 4600 60  0001 C CNN "Package ID"
F 11 "~" H 2900 4600 60  0001 C CNN "Source"
F 12 "MGSF2N02ELT1GOSCT-ND" H 0   0   50  0001 C CNN "digikey"
F 13 "1453618" H 0   0   50  0001 C CNN "farnell"
F 14 "BSS138" H 2900 4600 60  0001 C CNN "manf#"
F 15 "BSS138" H 0   0   50  0001 C CNN "part number"
F 16 "~" H 2900 4600 60  0001 C CNN "supplier1_link"
F 17 "~" H 2900 4600 60  0001 C CNN "supplier1_name"
F 18 "~" H 2900 4600 60  0001 C CNN "supplier2_link"
F 19 "~" H 2900 4600 60  0001 C CNN "supplier2_name"
	1    2900 4600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 5668EA21
P 3000 4950
F 0 "#PWR021" H 3000 4700 50  0001 C CNN
F 1 "GND" H 3000 4800 50  0000 C CNN
F 2 "" H 3000 4950 50  0000 C CNN
F 3 "" H 3000 4950 50  0000 C CNN
	1    3000 4950
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 569F9931
P 3200 4250
F 0 "R9" V 3280 4250 50  0000 C CNN
F 1 "15K" V 3200 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3130 4250 50  0001 C CNN
F 3 "" H 3200 4250 50  0000 C CNN
F 4 "15k 0603" H 0   0   50  0001 C CNN "Description"
F 5 "2502414" H 0   0   50  0001 C CNN "farnell"
F 6 "R6K015" H 0   0   50  0001 C CNN "internal p/n"
F 7 "WR06X1502FTL" H 0   0   50  0001 C CNN "manf#"
F 8 "WR06X1502FTL" H 0   0   50  0001 C CNN "part number"
	1    3200 4250
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 569F99E2
P 3500 4450
F 0 "R10" V 3580 4450 50  0000 C CNN
F 1 "10K" V 3500 4450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3430 4450 50  0001 C CNN
F 3 "" H 3500 4450 50  0000 C CNN
F 4 "10k 0603" H 0   0   50  0001 C CNN "Description"
F 5 "2447230" H 0   0   50  0001 C CNN "farnell"
F 6 "R6K010" H 0   0   50  0001 C CNN "internal p/n"
F 7 "MCWR06X1002FTL" H 0   0   50  0001 C CNN "manf#"
F 8 "MCWR06X1002FTL" H 0   0   50  0001 C CNN "part number"
	1    3500 4450
	-1   0    0    1   
$EndComp
$Comp
L R R1
U 1 1 569FB240
P 1600 6200
F 0 "R1" V 1680 6200 50  0000 C CNN
F 1 "100R" V 1600 6200 50  0000 C CNN
F 2 "Local_Resistors_SMD:R_0805" V 1530 6200 50  0001 C CNN
F 3 "" H 1600 6200 50  0000 C CNN
F 4 "100R 0805 0.1%" H 0   0   50  0001 C CNN "Description"
F 5 "1577644" H 0   0   50  0001 C CNN "farnell"
F 6 "R8R1000" H 0   0   50  0001 C CNN "internal p/n"
F 7 "ERA6AEB101V" H 0   0   50  0001 C CNN "manf#"
F 8 "ERA6AEB101V" H 0   0   50  0001 C CNN "part number"
	1    1600 6200
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NC_Small JP1
U 1 1 56A252A9
P 950 3150
F 0 "JP1" H 950 3230 50  0000 C CNN
F 1 "Jumper_NC_Small" H 960 3090 50  0001 C CNN
F 2 "local:NC_SMD_JPR" H 950 3150 50  0001 C CNN
F 3 "" H 950 3150 50  0000 C CNN
F 4 "empty" H 0   0   50  0001 C CNN "Description"
	1    950  3150
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NC_Small JP2
U 1 1 56A252E6
P 950 3300
F 0 "JP2" H 950 3380 50  0000 C CNN
F 1 "Jumper_NC_Small" H 960 3240 50  0001 C CNN
F 2 "local:NC_SMD_JPR" H 950 3300 50  0001 C CNN
F 3 "" H 950 3300 50  0000 C CNN
F 4 "empty" H 0   0   50  0001 C CNN "Description"
	1    950  3300
	1    0    0    -1  
$EndComp
$Comp
L LM358_DUAL U1
U 1 1 56B210A7
P 1850 6050
F 0 "U1" H 2000 6300 60  0000 R CNN
F 1 "LM358_DUAL" H 2000 6200 60  0000 R CNN
F 2 "local:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1850 6100 60  0001 C CNN
F 3 "" H 1850 6100 60  0000 C CNN
F 4 "LM358 op-amp" H 0   0   50  0001 C CNN "Description"
F 5 "2342290" H 0   0   50  0001 C CNN "farnell"
F 6 "LM358" H 0   0   50  0001 C CNN "internal p/n"
F 7 "LM358DRG3" H 0   0   50  0001 C CNN "manf#"
F 8 "LM358DRG3" H 0   0   50  0001 C CNN "part number"
	1    1850 6050
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NC_Small JP3
U 1 1 56B34327
P 1500 6600
F 0 "JP3" H 1500 6680 50  0000 C CNN
F 1 "Jumper_NC_Small" H 1510 6540 50  0001 C CNN
F 2 "local:NC_SMD_JPR" H 1500 6600 50  0001 C CNN
F 3 "" H 1500 6600 50  0000 C CNN
F 4 "empty" H 0   0   50  0001 C CNN "Description"
	1    1500 6600
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR022
U 1 1 56B36243
P 4700 4950
F 0 "#PWR022" H 4700 4700 50  0001 C CNN
F 1 "GND" H 4700 4800 50  0000 C CNN
F 2 "" H 4700 4950 50  0000 C CNN
F 3 "" H 4700 4950 50  0000 C CNN
	1    4700 4950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR023
U 1 1 56B36283
P 4700 4400
F 0 "#PWR023" H 4700 4250 50  0001 C CNN
F 1 "+5V" H 4700 4540 50  0000 C CNN
F 2 "" H 4700 4400 50  0000 C CNN
F 3 "" H 4700 4400 50  0000 C CNN
	1    4700 4400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P4
U 1 1 56B36331
P 4300 4700
F 0 "P4" H 4300 4900 50  0000 C CNN
F 1 "0-2V_ADC" V 4400 4700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 4300 4700 50  0001 C CNN
F 3 "" H 4300 4700 50  0000 C CNN
F 4 "3-pin female header" H 0   0   50  0001 C CNN "Description"
F 5 "Harwin" H 0   0   50  0001 C CNN "MFN"
F 6 "952-1784-ND" H 0   0   50  0001 C CNN "digikey"
F 7 "7991924" H 0   0   50  0001 C CNN "farnell"
F 8 "M20-7820346" H 0   0   50  0001 C CNN "manf#"
F 9 "M20-7820346" H 0   0   50  0001 C CNN "part number"
F 10 "681-6810" H 0   0   50  0001 C CNN "rs"
	1    4300 4700
	-1   0    0    1   
$EndComp
$Comp
L ATMEGA328P-A IC1
U 1 1 56F0C166
P 5900 2450
F 0 "IC1" H 5150 3700 50  0000 L BNN
F 1 "ATMEGA328P-A" H 6300 1050 50  0000 L BNN
F 2 "Housings_QFP:TQFP-32_7x7mm_Pitch0.8mm" H 5900 2450 50  0001 C CIN
F 3 "" H 5900 2450 50  0000 C CNN
F 4 "Atmega 328P-AUR" H 0   0   50  0001 C CNN "Description"
F 5 "ATMEGA328P-AURCT-ND" H 0   0   50  0001 C CNN "digikey"
F 6 "2425124" H 0   0   50  0001 C CNN "farnell"
F 7 "IC328PA" H 0   0   50  0001 C CNN "internal p/n"
F 8 "ATMEGA328P-AUR" H 0   0   50  0001 C CNN "manf#"
F 9 "ATMEGA328P-AUR" H 0   0   50  0001 C CNN "part number"
	1    5900 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 56F0C6FC
P 4900 3800
F 0 "#PWR024" H 4900 3550 50  0001 C CNN
F 1 "GND" H 4900 3650 50  0000 C CNN
F 2 "" H 4900 3800 50  0000 C CNN
F 3 "" H 4900 3800 50  0000 C CNN
	1    4900 3800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 P6
U 1 1 56F0D321
P 10700 3800
F 0 "P6" H 10700 4150 50  0000 C CNN
F 1 "FTDI_SLAVE_MC" V 10800 3800 50  0000 C CNN
F 2 "local:Pin_Header_Straight_1x06" H 10700 3800 50  0001 C CNN
F 3 "" H 10700 3800 50  0000 C CNN
F 4 "unpopulated" H 0   0   50  0001 C CNN "Description"
	1    10700 3800
	1    0    0    1   
$EndComp
$Comp
L CONN_02X03 P5
U 1 1 56F0D73D
P 8350 3950
F 0 "P5" H 8350 4150 50  0000 C CNN
F 1 "ICSP" H 8350 3750 50  0000 C CNN
F 2 "local:Pin_Header_Straight_2x03" H 8350 2750 50  0001 C CNN
F 3 "" H 8350 2750 50  0000 C CNN
F 4 "unpopulated" H 0   0   50  0001 C CNN "Description"
	1    8350 3950
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR025
U 1 1 56F0E88F
P 10400 3500
F 0 "#PWR025" H 10400 3350 50  0001 C CNN
F 1 "+5V" H 10400 3640 50  0000 C CNN
F 2 "" H 10400 3500 50  0000 C CNN
F 3 "" H 10400 3500 50  0000 C CNN
	1    10400 3500
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR026
U 1 1 56F0EAA1
P 4900 1200
F 0 "#PWR026" H 4900 1050 50  0001 C CNN
F 1 "+5V" H 4900 1340 50  0000 C CNN
F 2 "" H 4900 1200 50  0000 C CNN
F 3 "" H 4900 1200 50  0000 C CNN
	1    4900 1200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 56F0F5BD
P 10400 4100
F 0 "#PWR027" H 10400 3850 50  0001 C CNN
F 1 "GND" H 10400 3950 50  0000 C CNN
F 2 "" H 10400 4100 50  0000 C CNN
F 3 "" H 10400 4100 50  0000 C CNN
	1    10400 4100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR028
U 1 1 56F0F7D0
P 8650 4150
F 0 "#PWR028" H 8650 4000 50  0001 C CNN
F 1 "+5V" H 8650 4290 50  0000 C CNN
F 2 "" H 8650 4150 50  0000 C CNN
F 3 "" H 8650 4150 50  0000 C CNN
	1    8650 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 56F0FA48
P 8250 4350
F 0 "#PWR029" H 8250 4100 50  0001 C CNN
F 1 "GND" H 8250 4200 50  0000 C CNN
F 2 "" H 8250 4350 50  0000 C CNN
F 3 "" H 8250 4350 50  0000 C CNN
	1    8250 4350
	1    0    0    -1  
$EndComp
$Comp
L C_Small C2
U 1 1 56F128E3
P 4750 2350
F 0 "C2" H 4760 2420 50  0000 L CNN
F 1 "0.1uF" H 4760 2270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4750 2350 50  0001 C CNN
F 3 "" H 4750 2350 50  0000 C CNN
F 4 "100nF 0603" H 0   0   50  0001 C CNN "Description"
F 5 "any" H 0   0   50  0001 C CNN "Source"
F 6 "2112833" H 0   0   50  0001 C CNN "farnell"
F 7 "C6N100" H 0   0   50  0001 C CNN "internal p/n"
	1    4750 2350
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C1
U 1 1 56F1293E
P 4500 2350
F 0 "C1" H 4510 2420 50  0000 L CNN
F 1 "10uF" H 4510 2270 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_3x5.3" H 4500 2350 50  0001 C CNN
F 3 "" H 4500 2350 50  0000 C CNN
F 4 "10uF 3x5.3 V" H 0   0   50  0001 C CNN "Description"
F 5 "any" H 0   0   50  0001 C CNN "Source"
F 6 "493-2099-1-ND" H 0   0   50  0001 C CNN "digikey"
F 7 "8823014" H 0   0   50  0001 C CNN "farnell"
F 8 "CE3U010" H 0   0   50  0001 C CNN "internal p/n"
F 9 "UWX1C100MCL2GB" H 0   0   50  0001 C CNN "manf#"
F 10 "UWX1C100MCL2GB" H 0   0   50  0001 C CNN "part number"
F 11 "739-5557" H 0   0   50  0001 C CNN "rs"
	1    4500 2350
	1    0    0    -1  
$EndComp
$Comp
L R_Small R2
U 1 1 57326D88
P 7950 2800
F 0 "R2" H 7980 2820 50  0000 L CNN
F 1 "10K" H 7980 2760 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 7950 2800 50  0001 C CNN
F 3 "" H 7950 2800 50  0000 C CNN
F 4 "10k 0603" H 0   0   50  0001 C CNN "Description"
F 5 "2447230" H 0   0   50  0001 C CNN "farnell"
F 6 "R6K010" H 0   0   50  0001 C CNN "internal p/n"
F 7 "MCWR06X1002FTL" H 0   0   50  0001 C CNN "manf#"
F 8 "MCWR06X1002FTL" H 0   0   50  0001 C CNN "part number"
	1    7950 2800
	1    0    0    -1  
$EndComp
$Comp
L C_Small C5
U 1 1 5732931D
P 9700 3550
F 0 "C5" H 9710 3620 50  0000 L CNN
F 1 "0.1uF" H 9710 3470 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 9700 3550 50  0001 C CNN
F 3 "" H 9700 3550 50  0000 C CNN
F 4 "100nF 0603" H 0   0   50  0001 C CNN "Description"
F 5 "any" H 0   0   50  0001 C CNN "Source"
F 6 "2112833" H 0   0   50  0001 C CNN "farnell"
F 7 "C6N100" H 0   0   50  0001 C CNN "internal p/n"
	1    9700 3550
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR030
U 1 1 5732709D
P 7950 2650
F 0 "#PWR030" H 7950 2500 50  0001 C CNN
F 1 "+5V" H 7950 2790 50  0000 C CNN
F 2 "" H 7950 2650 50  0000 C CNN
F 3 "" H 7950 2650 50  0000 C CNN
	1    7950 2650
	1    0    0    -1  
$EndComp
$Comp
L Resonator-RESCUE-red-peg Y2
U 1 1 57345E9E
P 7650 2050
F 0 "Y2" H 7450 2200 60  0000 C CNN
F 1 "Resonator" H 8150 1800 60  0000 C CNN
F 2 "local:Resonator_3.2x1.3" H 7650 1950 60  0001 C CNN
F 3 "" H 7650 1950 60  0000 C CNN
F 4 "16MHz resonator" H 0   0   50  0001 C CNN "Description"
F 5 "RS" H 0   0   50  0001 C CNN "Source"
F 6 "490-1198-1-ND" H 0   0   50  0001 C CNN "digikey"
F 7 "2443265" H 0   0   50  0001 C CNN "farnell"
F 8 "XS16M" H 0   0   50  0001 C CNN "internal p/n"
F 9 "624-1077" H 0   0   50  0001 C CNN "rs"
	1    7650 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 5734620B
P 7650 2700
F 0 "#PWR031" H 7650 2450 50  0001 C CNN
F 1 "GND" H 7650 2550 50  0000 C CNN
F 2 "" H 7650 2700 50  0000 C CNN
F 3 "" H 7650 2700 50  0000 C CNN
	1    7650 2700
	1    0    0    -1  
$EndComp
Text GLabel 4900 6800 2    60   Input ~ 0
SCL
Text GLabel 4900 6950 2    60   Input ~ 0
SDA
Text GLabel 3500 6400 0    60   Input ~ 0
A1
Text Notes 2850 5400 2    60   ~ 0
4-20mA differential input
Text GLabel 7250 2700 2    60   Input ~ 0
SCL
Text GLabel 7000 2600 2    60   Input ~ 0
SDA
Text GLabel 8900 5650 0    60   Input ~ 0
SCL
Text GLabel 8900 5500 0    60   Input ~ 0
SDA
Text Notes 9450 4950 2    60   ~ 0
Real Time Clock
Text GLabel 8650 2000 0    60   Input ~ 0
CS_SD
Text GLabel 8650 1800 0    60   Input ~ 0
MOSI
Text GLabel 8650 1600 0    60   Input ~ 0
SCK
Text GLabel 10100 2350 0    60   Input ~ 0
MISO
Text GLabel 3050 2750 2    60   Input ~ 0
CS_SD
Text GLabel 3050 2050 2    60   Input ~ 0
MOSI
Text GLabel 3050 1850 2    60   Input ~ 0
SCK
Text GLabel 3350 1950 2    60   Input ~ 0
MISO
Text Notes 2200 2200 0    60   ~ 0
CS_NET
Text Notes 2100 2900 0    60   ~ 0
CS_NET_SD
Text Notes 2250 3000 0    60   ~ 0
GSM_TX
Text Notes 2250 3100 0    60   ~ 0
GSM_RX
Text Notes 9650 950  0    60   ~ 0
microSD card reader
Text Notes 2000 2300 0    60   ~ 0
PCINT0_vect
Text GLabel 3200 2200 2    60   Input ~ 0
SDI-12
Text Notes 2600 3250 2    60   ~ 0
HW_SERIAL
Text GLabel 1400 5600 2    60   Input ~ 0
SW_PWR
Text Notes 2550 2600 2    60   ~ 0
GSM_ACT
Text GLabel 1250 4200 2    60   Input ~ 0
SW_PWR
Text GLabel 1250 4400 2    60   Input ~ 0
SDI-12
Text Notes 1500 4100 2    60   ~ 0
SDI-12 input
Text GLabel 7850 5550 2    60   Input ~ 0
SW_PWR
Text GLabel 5950 5350 0    60   Input ~ 0
SENSOR_ACTIVE
Text GLabel 3650 4250 2    60   Input ~ 0
A1
Text GLabel 2650 4600 0    60   Input ~ 0
SENSOR_ACTIVE
Text GLabel 3400 2650 2    60   Input ~ 0
SENSOR_ACTIVE
Text Notes 3150 4150 0    60   ~ 0
5V to 2V divider
Text Notes 4350 7500 0    60   ~ 0
N.B. max 2V differential input
Text GLabel 800  3300 0    60   Input ~ 0
SCL
Text GLabel 800  3150 0    60   Input ~ 0
SDA
Text GLabel 3400 6600 0    60   Input ~ 0
A2
Text GLabel 4800 4700 2    60   Input ~ 0
A2
Text Notes 5650 1100 0    60   ~ 0
slave microcontroller
Text GLabel 8050 3000 2    60   Input ~ 0
RST
Text GLabel 7100 2950 2    60   Input ~ 0
RX
Text GLabel 6950 3050 2    60   Input ~ 0
TX
Text GLabel 9450 3550 0    60   Input ~ 0
RST
Text GLabel 10150 3650 0    60   Input ~ 0
TX
Text GLabel 10350 3750 0    60   Input ~ 0
RX
Text GLabel 8450 4350 2    60   Input ~ 0
MOSI
Text GLabel 8500 3600 2    60   Input ~ 0
MISO
Text GLabel 8200 3600 0    60   Input ~ 0
RST
Text GLabel 8450 3450 2    60   Input ~ 0
SCK
Text GLabel 7000 1650 2    60   Input ~ 0
MOSI
Text GLabel 7300 1750 2    60   Input ~ 0
MISO
Text GLabel 7000 1850 2    60   Input ~ 0
SCK
Text GLabel 7350 1550 2    60   Input ~ 0
SLV_SS
Text GLabel 3050 2350 2    60   Input ~ 0
SLV_SS
Text GLabel 7100 3150 2    60   Input ~ 0
SENSOR_ACTIVE
Text GLabel 6950 1450 2    60   Input ~ 0
SDI-12
Text GLabel 7050 3450 2    60   Input ~ 0
CS_SD
Text GLabel 3450 1550 2    60   Input ~ 0
SDA
Text GLabel 3450 1400 2    60   Input ~ 0
SCL
Wire Wire Line
	1200 6150 1400 6150
Wire Wire Line
	1400 6150 1400 6000
Wire Wire Line
	1200 6250 1400 6250
Wire Wire Line
	1400 6250 1400 6400
Wire Wire Line
	4200 5500 4200 5600
Wire Wire Line
	2350 5500 4850 5500
Wire Wire Line
	4800 6600 4850 6600
Wire Wire Line
	4850 6600 4850 5500
Wire Wire Line
	4800 6800 4900 6800
Wire Wire Line
	4800 6900 4850 6900
Wire Wire Line
	4850 6900 4850 6950
Wire Wire Line
	4850 6950 4900 6950
Wire Wire Line
	3600 6500 3450 6500
Wire Wire Line
	3500 6400 3600 6400
Wire Wire Line
	4200 7500 4200 7550
Wire Wire Line
	3000 1450 3050 1450
Wire Wire Line
	3050 1450 3050 1400
Wire Wire Line
	3050 1400 3450 1400
Wire Wire Line
	3000 1550 3450 1550
Wire Wire Line
	950  1950 950  2150
Wire Wire Line
	950  2150 1100 2150
Wire Wire Line
	850  1700 850  2250
Wire Wire Line
	850  2250 1100 2250
Wire Wire Line
	1100 2350 900  2350
Wire Wire Line
	900  2350 900  2650
Wire Wire Line
	1100 2450 900  2450
Wire Wire Line
	10350 5600 10250 5600
Wire Wire Line
	10350 5500 10350 5600
Wire Wire Line
	10350 5500 10550 5500
Wire Wire Line
	10550 5500 10550 5550
Wire Wire Line
	10250 5700 10350 5700
Wire Wire Line
	10350 5700 10350 5800
Wire Wire Line
	10350 5800 10550 5800
Wire Wire Line
	10550 5800 10550 5750
Wire Wire Line
	9650 4700 9650 4800
Wire Wire Line
	9750 4800 9750 4750
Wire Wire Line
	9750 4750 10400 4750
Wire Wire Line
	10400 4750 10400 4800
Wire Wire Line
	10400 5100 10400 5100
Wire Wire Line
	9650 6300 9650 6350
Wire Wire Line
	9050 5500 8900 5500
Wire Wire Line
	9050 5600 9000 5600
Wire Wire Line
	9000 5600 9000 5650
Wire Wire Line
	9000 5650 8900 5650
Wire Wire Line
	3150 1050 3150 1000
Wire Wire Line
	3150 1000 3350 1000
Wire Wire Line
	3250 1000 3250 900 
Wire Wire Line
	3350 1000 3350 1050
Wire Wire Line
	3150 1350 3150 1400
Wire Wire Line
	3350 1350 3350 1550
Wire Wire Line
	10150 2000 10250 2000
Wire Wire Line
	10250 1900 10200 1900
Wire Wire Line
	10200 1900 10200 2900
Wire Wire Line
	10200 1200 10200 1700
Wire Wire Line
	10200 1700 10250 1700
Wire Wire Line
	9950 2200 10100 2200
Wire Wire Line
	10000 1500 10250 1500
Wire Wire Line
	9950 2100 10050 2100
Wire Wire Line
	10050 2100 10050 1600
Wire Wire Line
	10050 1600 10250 1600
Wire Wire Line
	9950 2000 10000 2000
Wire Wire Line
	10100 1800 10250 1800
Wire Wire Line
	8850 1500 8750 1500
Wire Wire Line
	8750 1500 8750 2850
Wire Wire Line
	8850 1700 8750 1700
Wire Wire Line
	8850 2000 8750 2000
Wire Wire Line
	8850 1600 8650 1600
Wire Wire Line
	8850 1800 8650 1800
Wire Wire Line
	8850 1900 8700 1900
Wire Wire Line
	8700 1900 8700 2000
Wire Wire Line
	8700 2000 8650 2000
Wire Wire Line
	9450 850  9450 900 
Wire Wire Line
	9450 2800 9450 2950
Wire Wire Line
	3000 1850 3050 1850
Wire Wire Line
	3000 1950 3350 1950
Wire Wire Line
	3000 2050 3050 2050
Wire Wire Line
	3050 2750 3000 2750
Wire Wire Line
	1200 6350 1350 6350
Wire Wire Line
	1350 6350 1350 6800
Wire Wire Line
	1200 6050 1300 6050
Wire Wire Line
	1300 6050 1300 5600
Wire Wire Line
	1150 4300 1200 4300
Wire Wire Line
	1200 4300 1200 4200
Wire Wire Line
	1200 4200 1250 4200
Wire Wire Line
	1150 4500 1200 4500
Wire Wire Line
	1200 4500 1200 4600
Wire Wire Line
	1150 4400 1250 4400
Wire Wire Line
	700  2550 1100 2550
Wire Wire Line
	6950 4550 6950 4750
Wire Wire Line
	7050 4750 7050 4650
Wire Wire Line
	7050 4650 6950 4650
Wire Wire Line
	7650 5550 7850 5550
Wire Wire Line
	7650 5450 7750 5450
Wire Wire Line
	7750 5450 7750 5550
Wire Wire Line
	6950 6150 6950 6300
Wire Wire Line
	6050 6000 6050 6200
Wire Wire Line
	6050 6200 6950 6200
Wire Wire Line
	5950 5350 6150 5350
Wire Wire Line
	6050 5700 6050 5350
Wire Wire Line
	2650 4600 2700 4600
Wire Wire Line
	2750 4350 3000 4350
Wire Wire Line
	3000 4350 3000 4400
Wire Wire Line
	2750 4250 3050 4250
Wire Wire Line
	3000 4050 3000 4150
Wire Wire Line
	3000 4150 2750 4150
Wire Wire Line
	3000 4800 3000 4950
Wire Wire Line
	3000 2350 3050 2350
Wire Wire Line
	10000 2000 10000 1500
Wire Wire Line
	10100 2200 10100 1800
Wire Wire Line
	3350 4250 3650 4250
Wire Wire Line
	3500 4300 3500 4250
Wire Wire Line
	3500 4600 3500 4850
Wire Wire Line
	3500 4850 3000 4850
Wire Wire Line
	3450 6500 3450 7150
Wire Wire Line
	1850 6100 1800 6100
Wire Wire Line
	1800 6100 1800 6200
Wire Wire Line
	1800 6200 3100 6200
Wire Wire Line
	3100 6200 3100 6050
Wire Wire Line
	3050 6050 3250 6050
Wire Wire Line
	1850 6450 1800 6450
Wire Wire Line
	1800 6450 1800 6550
Wire Wire Line
	1800 6550 3100 6550
Wire Wire Line
	3100 6550 3100 6400
Wire Wire Line
	3050 6400 3250 6400
Wire Wire Line
	1400 6000 1850 6000
Wire Wire Line
	1600 6050 1600 6000
Wire Wire Line
	1400 6400 1750 6400
Wire Wire Line
	1750 6400 1750 6350
Wire Wire Line
	1750 6350 1850 6350
Wire Wire Line
	1600 6350 1600 6400
Wire Wire Line
	3250 6050 3250 6200
Wire Wire Line
	3250 6200 3600 6200
Wire Wire Line
	3250 6400 3250 6300
Wire Wire Line
	3250 6300 3600 6300
Wire Wire Line
	3350 5500 3350 5450
Wire Wire Line
	2350 5550 2350 5500
Wire Wire Line
	2350 6950 2350 7050
Wire Wire Line
	2350 7050 3450 7050
Wire Wire Line
	800  3150 850  3150
Wire Wire Line
	800  3300 850  3300
Wire Wire Line
	1050 3150 1100 3150
Wire Wire Line
	1050 3300 1050 3300
Wire Wire Line
	1050 3300 1050 3250
Wire Wire Line
	1050 3250 1100 3250
Wire Wire Line
	1500 6500 1500 6400
Wire Wire Line
	1500 6700 1500 6750
Wire Wire Line
	1500 6750 1350 6750
Wire Wire Line
	3400 6600 3600 6600
Wire Wire Line
	4500 4600 4700 4600
Wire Wire Line
	4700 4600 4700 4400
Wire Wire Line
	4500 4700 4800 4700
Wire Wire Line
	4500 4800 4700 4800
Wire Wire Line
	4700 4800 4700 4950
Wire Wire Line
	700  1450 700  2550
Wire Wire Line
	3000 2250 3100 2250
Wire Wire Line
	3100 2250 3100 2200
Wire Wire Line
	3100 2200 3200 2200
Wire Wire Line
	3000 1750 3750 1750
Wire Wire Line
	3750 1750 3750 1850
Wire Wire Line
	10150 2000 10150 2350
Wire Wire Line
	10150 2350 10100 2350
Wire Wire Line
	10200 2900 9450 2900
Wire Wire Line
	8750 2850 9450 2850
Wire Wire Line
	5000 1350 4900 1350
Wire Wire Line
	4900 1650 4900 1200
Wire Wire Line
	4900 1450 5000 1450
Wire Wire Line
	4500 1650 5000 1650
Wire Wire Line
	4900 3650 5000 3650
Wire Wire Line
	4900 3450 4900 3800
Wire Wire Line
	5000 3550 4900 3550
Wire Wire Line
	4750 3450 5000 3450
Wire Wire Line
	6900 2700 7250 2700
Wire Wire Line
	6900 2600 7000 2600
Wire Wire Line
	7100 2950 6900 2950
Wire Wire Line
	6900 3050 6950 3050
Wire Wire Line
	10500 3850 10400 3850
Wire Wire Line
	10400 3850 10400 3500
Wire Wire Line
	9800 3550 10500 3550
Wire Wire Line
	10150 3650 10500 3650
Wire Wire Line
	10350 3750 10500 3750
Wire Wire Line
	10500 4050 10400 4050
Wire Wire Line
	10400 4050 10400 4100
Wire Wire Line
	8450 4200 8450 4250
Wire Wire Line
	8450 4250 8650 4250
Wire Wire Line
	8650 4250 8650 4150
Wire Wire Line
	8450 4350 8350 4350
Wire Wire Line
	8350 4350 8350 4200
Wire Wire Line
	8250 4350 8250 4200
Wire Wire Line
	8500 3600 8450 3600
Wire Wire Line
	8450 3600 8450 3700
Wire Wire Line
	8200 3600 8250 3600
Wire Wire Line
	8250 3600 8250 3700
Wire Wire Line
	8450 3450 8350 3450
Wire Wire Line
	8350 3450 8350 3700
Wire Wire Line
	6900 1550 7350 1550
Wire Wire Line
	6900 1650 7000 1650
Wire Wire Line
	6900 1750 7300 1750
Wire Wire Line
	6900 1850 7000 1850
Wire Wire Line
	3000 2650 3400 2650
Wire Wire Line
	7100 3150 6900 3150
Wire Wire Line
	4750 2250 4750 1650
Wire Wire Line
	4500 2250 4500 1650
Wire Wire Line
	4750 2450 4750 3450
Wire Wire Line
	4500 2450 4500 3400
Wire Wire Line
	4500 3400 4750 3400
Wire Wire Line
	6950 1450 6900 1450
Wire Wire Line
	7050 3450 6900 3450
Wire Wire Line
	1300 5600 1400 5600
Wire Wire Line
	3600 6700 3450 6700
Wire Wire Line
	6900 2800 7550 2800
Wire Wire Line
	7550 2800 7550 3000
Wire Wire Line
	7550 3000 8050 3000
Wire Wire Line
	7950 3000 7950 2900
Wire Wire Line
	7950 2650 7950 2700
Wire Wire Line
	9450 3550 9600 3550
Wire Wire Line
	6900 2050 7250 2050
Wire Wire Line
	6900 1950 7300 1950
Wire Wire Line
	7300 1950 7300 1850
Wire Wire Line
	7300 1850 8100 1850
Wire Wire Line
	8100 1850 8100 2000
Wire Wire Line
	8100 2000 8050 2000
Wire Wire Line
	7650 2650 7650 2700
Connection ~ 4200 5500
Connection ~ 900  2450
Connection ~ 3250 1000
Connection ~ 3150 1400
Connection ~ 3350 1550
Connection ~ 8750 1700
Connection ~ 8750 2000
Connection ~ 6950 4650
Connection ~ 7750 5550
Connection ~ 6950 6200
Connection ~ 6050 5350
Connection ~ 3500 4250
Connection ~ 3000 4850
Connection ~ 1600 6000
Connection ~ 1600 6400
Connection ~ 3100 6050
Connection ~ 3100 6400
Connection ~ 3350 5500
Connection ~ 3450 7050
Connection ~ 1500 6400
Connection ~ 1350 6750
Connection ~ 9450 2900
Connection ~ 9450 2850
Connection ~ 4900 1350
Connection ~ 4900 1450
Connection ~ 4900 3650
Connection ~ 4900 3550
Connection ~ 4900 1650
Connection ~ 4750 1650
Connection ~ 4900 3450
Connection ~ 4750 3400
Connection ~ 3450 6700
Connection ~ 7950 3000
NoConn ~ 4800 6700
NoConn ~ 3600 6800
NoConn ~ 3600 6900
NoConn ~ 3000 1650
NoConn ~ 9050 5700
NoConn ~ 8850 2100
NoConn ~ 9950 1900
NoConn ~ 8850 2200
NoConn ~ 3000 2150
NoConn ~ 3000 2850
NoConn ~ 3000 2950
NoConn ~ 3000 3050
NoConn ~ 3000 3150
NoConn ~ 3000 3250
NoConn ~ 3000 2550
NoConn ~ 6150 5450
NoConn ~ 6150 5550
NoConn ~ 1100 2750
NoConn ~ 1100 2850
NoConn ~ 1100 2950
NoConn ~ 1100 3050
NoConn ~ 5000 2700
NoConn ~ 5000 2800
NoConn ~ 5000 1950
NoConn ~ 6900 2200
NoConn ~ 6900 2300
NoConn ~ 6900 2400
NoConn ~ 6900 2500
NoConn ~ 10500 3950
NoConn ~ 6900 1350
NoConn ~ 6900 3250
NoConn ~ 6900 3350
NoConn ~ 6900 3550
NoConn ~ 6900 3650
NoConn ~ 1100 2050
$EndSCHEMATC
