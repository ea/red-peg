EESchema Schematic File Version 2
LIBS:red-peg-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:red-peg
LIBS:red-peg-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Battery-RESCUE-red-peg BATTERY1
U 1 1 57DA7F2F
P 2400 1400
F 0 "BATTERY1" H 2500 1450 50  0000 L CNN
F 1 "CR1220 Battery" H 2500 1350 50  0000 L CNN
F 2 "" V 2400 1440 50  0000 C CNN
F 3 "" V 2400 1440 50  0000 C CNN
F 4 "~" H 2400 1400 60  0001 C CNN "Characteristics"
F 5 "~" H 2400 1400 60  0001 C CNN "Description"
F 6 "~" H 2400 1400 60  0001 C CNN "Package ID"
F 7 "~" H 2400 1400 60  0001 C CNN "Source"
F 8 "~" H 2400 1400 60  0001 C CNN "Critical"
F 9 "~" H 2400 1400 60  0001 C CNN "Notes"
F 10 "~" H 2400 1400 60  0001 C CNN "MFP"
F 11 "2065165" H 2400 1400 60  0001 C CNN "farnell"
F 12 "~" H 2400 1400 60  0001 C CNN "supplier2_name"
F 13 "~" H 2400 1400 60  0001 C CNN "supplier1_link"
F 14 "~" H 2400 1400 60  0001 C CNN "supplier2_link"
F 15 "Multicomp" H 2400 1400 60  0001 C CNN "MFN"
	1    2400 1400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P11
U 1 1 57DA7F6E
P 3500 1400
F 0 "P11" H 3500 1500 50  0000 C CNN
F 1 "Grey" V 3600 1400 50  0000 C CNN
F 2 "" H 3500 1400 50  0000 C CNN
F 3 "" H 3500 1400 50  0000 C CNN
F 4 "~" H 3500 1400 60  0001 C CNN "Characteristics"
F 5 "~" H 3500 1400 60  0001 C CNN "Description"
F 6 "~" H 3500 1400 60  0001 C CNN "Package ID"
F 7 "~" H 3500 1400 60  0001 C CNN "Source"
F 8 "~" H 3500 1400 60  0001 C CNN "Critical"
F 9 "~" H 3500 1400 60  0001 C CNN "Notes"
F 10 "256-742" H 3500 1400 60  0001 C CNN "MFP"
F 11 "~" H 3500 1400 60  0001 C CNN "supplier1_name"
F 12 "~" H 3500 1400 60  0001 C CNN "supplier2_name"
F 13 "~" H 3500 1400 60  0001 C CNN "supplier1_link"
F 14 "~" H 3500 1400 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 3500 1400 60  0001 C CNN "MFN"
F 16 "4015393" H 3500 1400 60  0001 C CNN "farnell"
	1    3500 1400
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P12
U 1 1 57DA7FB3
P 4000 1400
F 0 "P12" H 4000 1500 50  0000 C CNN
F 1 "Green" V 4100 1400 50  0000 C CNN
F 2 "" H 4000 1400 50  0000 C CNN
F 3 "" H 4000 1400 50  0000 C CNN
F 4 "~" H 4000 1400 60  0001 C CNN "Characteristics"
F 5 "~" H 4000 1400 60  0001 C CNN "Description"
F 6 "~" H 4000 1400 60  0001 C CNN "Package ID"
F 7 "~" H 4000 1400 60  0001 C CNN "Source"
F 8 "~" H 4000 1400 60  0001 C CNN "Critical"
F 9 "~" H 4000 1400 60  0001 C CNN "Notes"
F 10 "256-747" H 4000 1400 60  0001 C CNN "MFP"
F 11 "4015435" H 4000 1400 60  0001 C CNN "farnell"
F 12 "~" H 4000 1400 60  0001 C CNN "supplier2_name"
F 13 "~" H 4000 1400 60  0001 C CNN "supplier1_link"
F 14 "~" H 4000 1400 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 4000 1400 60  0001 C CNN "MFN"
	1    4000 1400
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P13
U 1 1 57DA7FDE
P 4500 1400
F 0 "P13" H 4500 1500 50  0000 C CNN
F 1 "Blue" V 4600 1400 50  0000 C CNN
F 2 "" H 4500 1400 50  0000 C CNN
F 3 "" H 4500 1400 50  0000 C CNN
F 4 "~" H 4500 1400 60  0001 C CNN "Characteristics"
F 5 "~" H 4500 1400 60  0001 C CNN "Description"
F 6 "~" H 4500 1400 60  0001 C CNN "Package ID"
F 7 "~" H 4500 1400 60  0001 C CNN "Source"
F 8 "~" H 4500 1400 60  0001 C CNN "Critical"
F 9 "~" H 4500 1400 60  0001 C CNN "Notes"
F 10 "256-744" H 4500 1400 60  0001 C CNN "MFP"
F 11 "4015411" H 4500 1400 60  0001 C CNN "farnell"
F 12 "~" H 4500 1400 60  0001 C CNN "supplier2_name"
F 13 "~" H 4500 1400 60  0001 C CNN "supplier1_link"
F 14 "~" H 4500 1400 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 4500 1400 60  0001 C CNN "MFN"
	1    4500 1400
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P14
U 1 1 57DA8008
P 5000 1400
F 0 "P14" H 5000 1500 50  0000 C CNN
F 1 "Orange" V 5100 1400 50  0000 C CNN
F 2 "" H 5000 1400 50  0000 C CNN
F 3 "" H 5000 1400 50  0000 C CNN
F 4 "~" H 5000 1400 60  0001 C CNN "Characteristics"
F 5 "~" H 5000 1400 60  0001 C CNN "Description"
F 6 "~" H 5000 1400 60  0001 C CNN "Package ID"
F 7 "~" H 5000 1400 60  0001 C CNN "Source"
F 8 "~" H 5000 1400 60  0001 C CNN "Critical"
F 9 "~" H 5000 1400 60  0001 C CNN "Notes"
F 10 "256-746" H 5000 1400 60  0001 C CNN "MFP"
F 11 "4015423" H 5000 1400 60  0001 C CNN "farnell"
F 12 "~" H 5000 1400 60  0001 C CNN "supplier2_name"
F 13 "~" H 5000 1400 60  0001 C CNN "supplier1_link"
F 14 "~" H 5000 1400 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 5000 1400 60  0001 C CNN "MFN"
	1    5000 1400
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P21
U 1 1 57DA8D1B
P 3750 1850
F 0 "P21" H 3750 1950 50  0000 C CNN
F 1 "Grey" V 3850 1850 50  0000 C CNN
F 2 "" H 3750 1850 50  0000 C CNN
F 3 "" H 3750 1850 50  0000 C CNN
F 4 "~" H 3750 1850 60  0001 C CNN "Characteristics"
F 5 "~" H 3750 1850 60  0001 C CNN "Description"
F 6 "~" H 3750 1850 60  0001 C CNN "Package ID"
F 7 "~" H 3750 1850 60  0001 C CNN "Source"
F 8 "~" H 3750 1850 60  0001 C CNN "Critical"
F 9 "~" H 3750 1850 60  0001 C CNN "Notes"
F 10 "256-742" H 3750 1850 60  0001 C CNN "MFP"
F 11 "~" H 3750 1850 60  0001 C CNN "supplier1_name"
F 12 "~" H 3750 1850 60  0001 C CNN "supplier2_name"
F 13 "~" H 3750 1850 60  0001 C CNN "supplier1_link"
F 14 "~" H 3750 1850 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 3750 1850 60  0001 C CNN "MFN"
F 16 "4015393" H 3750 1850 60  0001 C CNN "farnell"
	1    3750 1850
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P22
U 1 1 57DA8D3F
P 4250 1850
F 0 "P22" H 4250 1950 50  0000 C CNN
F 1 "Blue" V 4350 1850 50  0000 C CNN
F 2 "" H 4250 1850 50  0000 C CNN
F 3 "" H 4250 1850 50  0000 C CNN
F 4 "~" H 4250 1850 60  0001 C CNN "Characteristics"
F 5 "~" H 4250 1850 60  0001 C CNN "Description"
F 6 "~" H 4250 1850 60  0001 C CNN "Package ID"
F 7 "~" H 4250 1850 60  0001 C CNN "Source"
F 8 "~" H 4250 1850 60  0001 C CNN "Critical"
F 9 "~" H 4250 1850 60  0001 C CNN "Notes"
F 10 "256-744" H 4250 1850 60  0001 C CNN "MFP"
F 11 "4015411" H 4250 1850 60  0001 C CNN "farnell"
F 12 "~" H 4250 1850 60  0001 C CNN "supplier2_name"
F 13 "~" H 4250 1850 60  0001 C CNN "supplier1_link"
F 14 "~" H 4250 1850 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 4250 1850 60  0001 C CNN "MFN"
	1    4250 1850
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P23
U 1 1 57DA8D51
P 4750 1850
F 0 "P23" H 4750 1950 50  0000 C CNN
F 1 "Orange" V 4850 1850 50  0000 C CNN
F 2 "" H 4750 1850 50  0000 C CNN
F 3 "" H 4750 1850 50  0000 C CNN
F 4 "~" H 4750 1850 60  0001 C CNN "Characteristics"
F 5 "~" H 4750 1850 60  0001 C CNN "Description"
F 6 "~" H 4750 1850 60  0001 C CNN "Package ID"
F 7 "~" H 4750 1850 60  0001 C CNN "Source"
F 8 "~" H 4750 1850 60  0001 C CNN "Critical"
F 9 "~" H 4750 1850 60  0001 C CNN "Notes"
F 10 "256-746" H 4750 1850 60  0001 C CNN "MFP"
F 11 "4015423" H 4750 1850 60  0001 C CNN "farnell"
F 12 "~" H 4750 1850 60  0001 C CNN "supplier2_name"
F 13 "~" H 4750 1850 60  0001 C CNN "supplier1_link"
F 14 "~" H 4750 1850 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 4750 1850 60  0001 C CNN "MFN"
	1    4750 1850
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P31
U 1 1 57DA8E61
P 3750 2300
F 0 "P31" H 3750 2400 50  0000 C CNN
F 1 "Grey" V 3850 2300 50  0000 C CNN
F 2 "" H 3750 2300 50  0000 C CNN
F 3 "" H 3750 2300 50  0000 C CNN
F 4 "~" H 3750 2300 60  0001 C CNN "Characteristics"
F 5 "~" H 3750 2300 60  0001 C CNN "Description"
F 6 "~" H 3750 2300 60  0001 C CNN "Package ID"
F 7 "~" H 3750 2300 60  0001 C CNN "Source"
F 8 "~" H 3750 2300 60  0001 C CNN "Critical"
F 9 "~" H 3750 2300 60  0001 C CNN "Notes"
F 10 "256-742" H 3750 2300 60  0001 C CNN "MFP"
F 11 "~" H 3750 2300 60  0001 C CNN "supplier1_name"
F 12 "~" H 3750 2300 60  0001 C CNN "supplier2_name"
F 13 "~" H 3750 2300 60  0001 C CNN "supplier1_link"
F 14 "~" H 3750 2300 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 3750 2300 60  0001 C CNN "MFN"
F 16 "4015393" H 3750 2300 60  0001 C CNN "farnell"
	1    3750 2300
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P32
U 1 1 57DA8E73
P 4250 2300
F 0 "P32" H 4250 2400 50  0000 C CNN
F 1 "Green" V 4350 2300 50  0000 C CNN
F 2 "" H 4250 2300 50  0000 C CNN
F 3 "" H 4250 2300 50  0000 C CNN
F 4 "~" H 4250 2300 60  0001 C CNN "Characteristics"
F 5 "~" H 4250 2300 60  0001 C CNN "Description"
F 6 "~" H 4250 2300 60  0001 C CNN "Package ID"
F 7 "~" H 4250 2300 60  0001 C CNN "Source"
F 8 "~" H 4250 2300 60  0001 C CNN "Critical"
F 9 "~" H 4250 2300 60  0001 C CNN "Notes"
F 10 "256-747" H 4250 2300 60  0001 C CNN "MFP"
F 11 "4015435" H 4250 2300 60  0001 C CNN "farnell"
F 12 "~" H 4250 2300 60  0001 C CNN "supplier2_name"
F 13 "~" H 4250 2300 60  0001 C CNN "supplier1_link"
F 14 "~" H 4250 2300 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 4250 2300 60  0001 C CNN "MFN"
	1    4250 2300
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 P33
U 1 1 57DA8E97
P 4750 2300
F 0 "P33" H 4750 2400 50  0000 C CNN
F 1 "Orange" V 4850 2300 50  0000 C CNN
F 2 "" H 4750 2300 50  0000 C CNN
F 3 "" H 4750 2300 50  0000 C CNN
F 4 "~" H 4750 2300 60  0001 C CNN "Characteristics"
F 5 "~" H 4750 2300 60  0001 C CNN "Description"
F 6 "~" H 4750 2300 60  0001 C CNN "Package ID"
F 7 "~" H 4750 2300 60  0001 C CNN "Source"
F 8 "~" H 4750 2300 60  0001 C CNN "Critical"
F 9 "~" H 4750 2300 60  0001 C CNN "Notes"
F 10 "256-746" H 4750 2300 60  0001 C CNN "MFP"
F 11 "~" H 4750 2300 60  0001 C CNN "supplier1_name"
F 12 "~" H 4750 2300 60  0001 C CNN "supplier2_name"
F 13 "~" H 4750 2300 60  0001 C CNN "supplier1_link"
F 14 "~" H 4750 2300 60  0001 C CNN "supplier2_link"
F 15 "Wago" H 4750 2300 60  0001 C CNN "MFN"
F 16 "4015423" H 4750 2300 60  0001 C CNN "farnell"
	1    4750 2300
	0    -1   -1   0   
$EndComp
Text Notes 3800 1150 0    60   ~ 0
Wago connector parts
Text Notes 1850 1050 0    60   ~ 0
CR1220 battery for the holder
NoConn ~ 2400 1250
NoConn ~ 2400 1550
NoConn ~ 3500 1600
NoConn ~ 4000 1600
NoConn ~ 4500 1600
NoConn ~ 5000 1600
NoConn ~ 3750 2050
NoConn ~ 4250 2050
NoConn ~ 4750 2050
NoConn ~ 3750 2500
NoConn ~ 4250 2500
NoConn ~ 4750 2500
$EndSCHEMATC
