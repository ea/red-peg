//#include <SPI.h>
//#include <SdFat.h>
#include <HttpClient.h>
#include "red_peg.h"
red_peg RP;

//#define USE_GSM

#ifdef USE_GSM
#include <GSM.h>
//PIN Number
#define PINNUMBER ""
//APN data
// these settings are for EE sim cards
#define GPRS_APN       "everywhere" // replace your GPRS APN
#define GPRS_LOGIN     "eesecure"    // replace with your GPRS login
#define GPRS_PASSWORD  "secure" // replace with your GPRS password
// initialize the library instance
GSMClient client;
GPRS gprs;
GSM gsmAccess;

#elif USE_ETHERNET
#include <Ethernet.h>
#include <EthernetClient.h>
#endif

void setup()
{
  // initialize serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  Serial.println("Starting SPI_master.");
  delay(100);

#ifdef USE_GSM
  // connection state
  boolean notConnected = true;
  // After starting the modem with GSM.begin()
  // attach the shield to the GPRS network wit  Serial.println();
  //h the APN, login and password
  while (notConnected)
  {
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY))
      notConnected = false;
    else
    {
      Serial.println("Not connected");
      delay(1000);
    }
  }
  Serial.print(F("gsm connected"));
#endif

  // start red_peg for recording data
  RP.begin();
}

void loop()
{
  RP.sensorsOn();
  t_SensorData temperature = RP.get(TMP);
  Serial.print(F("Got… "));
  RP.print_data(temperature);
  RP.sensorsOff();
  delay(5000);
}
