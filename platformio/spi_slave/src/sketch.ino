#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <red_peg_slave.h>
red_peg_slave RPS;
RTC_DS1307 rtc;
MCP342x adc = MCP342x(MCP3428);
SDI12_RP mySDI12(SDI_12_PIN);

#define SERIAL_REPORT
#define VERSION_NUMBER "0.2.0"

char sdi12_address = '0';
bool foundSdi = false;
volatile bool usingSDI = false;

void setup()
{
  //DDRD &= B00000011;       // set Arduino pins 2 to 7 as inputs, leaves 0 & 1 (RX & TX) as is
  //DDRB = B00000000;        // set pins 8 to 13 as inputs
  //PORTD |= B11111000;      // enable pullups on pins 2 to 7, leave pins 0 and 1 alone
  //PORTB |= B11111111;      // enable pullups on pins 8 to 13
#ifdef SERIAL_REPORT
  Serial.begin(BAUD);
  Serial.print(F("start red_peg_slave "));
  Serial.print(F(VERSION_NUMBER));
  Serial.println();
#endif

  // start the RTC
  if (!rtc.begin()) {
#ifdef SERIAL_REPORT
    Serial.println(F("No RTC"));
#endif
    while (1);
  }

  // check we have the expected ADC attached
  if (!adcPresent()) {
#ifdef SERIAL_REPORT
    Serial.println(F("No ADC"));
#endif
    die();
  } else {
#ifdef SERIAL_REPORT
    Serial.println(F("ADC OK"));
#endif
  }


  //mySDI12.begin();
  /*
  // turn on the sensors to see if SDI is present
  usingSDI = true;
  pinMode(SLAVE_SENSOR_ACTIVE_PIN, OUTPUT);
  digitalWrite(SLAVE_SENSOR_ACTIVE_PIN, HIGH);
  delay(300);
  mySDI12.begin();
  //delay(300); // allow things to settle
  // check addresses 0-9
  if (foundSdi == false) {
    for(char i = '0'; i <= '9'; i++) {
      if(checkActive(i)) {
        sdi12_address = i;
        foundSdi = true;
        i='9'+1;
      }
    }
  }
  // check addresses a-z
  if (foundSdi == false) {
    for(char i = 'a'; i <= 'z'; i++) {
      if(checkActive(i)) {
        sdi12_address = i;
        foundSdi = true;
        i='9'+1;
      }
    }
  }
  // check addresses A-Z
  if (foundSdi == false) {
    for(char i = 'A'; i <= 'Z'; i++) {
      if(checkActive(i)) {
        sdi12_address = i;
        foundSdi = true;
        i='9'+1;
      }
    }
  }
  digitalWrite(SLAVE_SENSOR_ACTIVE_PIN, LOW);
  pinMode(SLAVE_SENSOR_ACTIVE_PIN, INPUT);
  usingSDI = false;
#ifdef SERIAL_REPORT
  if (foundSdi) {
    Serial.print(F("Found SDI device at address: "));
    Serial.println(sdi12_address);
  } else {
    Serial.println(F("No SDI device found"));
  }
#endif
*/

  pciSetup(SS_SLAVE_PIN);
  // start the SPI slave
  RPS.begin();

#ifdef SERIAL_REPORT
  Serial.println(F("END setup"));
#endif
}

void pciSetup(byte pin)
{
    cli();
    *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
    PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
    PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
    sei();
}

ISR(PCINT0_vect)
{
  if (usingSDI == true) {
    mySDI12.handleInterrupt();
  } else {
    RPS.handle_interrupt();
  }
}

void loop()
{
  // process the incoming bytes and read the appropriate sensor
  // there's an incoming command; parse it.
  //Serial.println(F("i haz commands: "));
  if (RPS.available()) {
    t_SensorData next = RPS.read();
    #ifdef SERIAL_REPORT
    Serial.print(F("IN: "));
    RPS.print_data(next);
    #endif

    // build and queue the response
    DateTime now = rtc.now();
    t_SensorData the_response;
    // always add the time
    the_response.y = now.year();
    the_response.m = now.month();
    the_response.d = now.day();
    the_response.hh = now.hour();
    the_response.mm = now.minute();
    the_response.ss = now.second();
    // set initial zeros
    the_response.the_sensor = OK;
    //the_response.the_data = 0.0;
    the_response.the_reading = 0;
    switch (next.the_command) {
      case VERSION:
        the_response.the_sensor = VERSION;
        // return the library version number so we can check compatibility
        the_response.major = SEM_VER_MAJOR;
        the_response.minor = SEM_VER_MINOR;
        the_response.patch = SEM_VER_PATCH;
        the_response.metadata = SEM_VER_META;
        break;
      case RTC:
        // report the current time
        the_response.the_sensor = RTC;
        break;
      //case MA4_20:
      case ADC1:
        // get the most recent reading from ADC channel0
        the_response.the_sensor = ADC1;
        the_response.the_reading = adc.getAdc(MCP_ADC1);
        break;
      //case ANA:
      case ADC2:
        // get the most recent reading from ADC channel1
        the_response.the_sensor = ADC2;
        the_response.the_reading = adc.getAdc(MCP_ADC2);
        break;
      //case TMP:
      case ADC3:
        // get the most recent reading from ADC channel2
        the_response.the_sensor = ADC3;
        the_response.the_reading = adc.getAdc(MCP_ADC3);
        break;
      case SDI_12_FLOAT:
        //TODO: Add default SDI-12 return
        the_response.the_sensor = SDI_12_FLOAT;
        the_response.float_level = getSdiReading(sdi12_address);
        break;
      case SDI_12:
        //TODO: Add default SDI-12 return
        the_response.the_sensor = SDI_12;
        the_response.the_reading = long(getSdiReading(sdi12_address) * 1000.0);
        break;
      case SET_RTC:
        rtc.adjust(DateTime(next.y, next.m, next.d, next.hh, next.mm, next.ss));
        now = rtc.now();
        the_response.y = now.year();
        the_response.m = now.month();
        the_response.d = now.day();
        the_response.hh = now.hour();
        the_response.mm = now.minute();
        the_response.ss = now.second();
        the_response.the_sensor = SET_RTC;
        break;
      default:
        break;
    }
    RPS.write(the_response);

    // report the outgoing
    #ifdef SERIAL_REPORT
    Serial.print(F(" OUT: "));
    RPS.print_data(the_response);
    #endif
  }

  if (digitalRead(SLAVE_SENSOR_ACTIVE_PIN) == LOW) {
    // sensors are turned off; go to sleep
    #ifdef SERIAL_REPORT
    Serial.print("Sleep...zzz...");
    delay(100);
    #endif
    attachInterrupt(INT0, wakeInterrupt, HIGH);
    delay(100);
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_mode();
    sleep_disable();
    #ifdef SERIAL_REPORT
    Serial.println(" Awake!");
    #endif
    }
}

void die() {
  pinMode(LED_BUILTIN, OUTPUT);
  while(1) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
  }
}

uint8_t adcPresent() {
  MCP342x::generalCallReset();
  delay(1); // MC342x needs 300us to settle, wait 1ms

  Wire.requestFrom(0x6F, (uint8_t)1);
  if (!Wire.available()) {
    return 0;
  } else {
    return 1;
  }
}

void wakeInterrupt() {
  detachInterrupt(INT0);
}

// this checks for activity at a particular SDI-12 address
// expects a char, '0'-'9', 'a'-'z', or 'A'-'Z'
boolean checkActive(char i){

  String myCommand = "";
  myCommand = "";
  myCommand += (char) i;                 // sends basic 'acknowledge' command [address][!]
  myCommand += "!";

  for(int j = 0; j < 3; j++){            // goes through three rapid contact attempts
    mySDI12.sendCommand(myCommand);
    if(mySDI12.available()>1) break;
    delay(30);
  }
  if(mySDI12.available()>2){       // if it hears anything it assumes the address is occupied
    mySDI12.flush();
    return true;
  }
  else {   // otherwise it is vacant.
    mySDI12.flush();
  }
  return false;
}

float getSdiReading(char i) {
  mySDI12.begin();
  while (mySDI12.available()) {
    // flush the char buffer
    mySDI12.read();
  }
  delay(300);
  usingSDI = true;
  // get the first reading from the first found SDI-12 sensor address
  String command = "";
  command += i;
  command += "M!"; // SDI-12 measurement command format  [address]['M'][!]
  mySDI12.sendCommand(command);
  while(!mySDI12.available()>5); // wait for acknowlegement with format [address][ttt (3 char, seconds)][number of measurments available, 0-9]
  delay(500);

  char addr = mySDI12.read(); //consume address
  // find out how long we have to wait (in seconds).
  int wait = 0;
  wait += 100 * (mySDI12.read()-'0');
  wait += 10 * (mySDI12.read()-'0');
  wait += 1 * (mySDI12.read()-'0');
  wait = constrain(wait, 4, 10);
  //Serial.println(wait);

  mySDI12.read(); // ignore # measurements, use only the first
  mySDI12.read(); // ignore carriage returnbyte
  mySDI12.read(); // ignore line feed
  //mySDI12.flush();
  while (mySDI12.available()) {
    mySDI12.read();
  }

  // in this example we will only take the 'DO' measurement
  command = "";
  command += i;
  /* for the GenBox signal generator:
   *  "D0!" will get the average reading
   *  "D1!" will return range for each reading
   */
  command += "D0!"; // SDI-12 command to get data [address][D][dataOption][!]
  mySDI12.sendCommand(command);
  while(!mySDI12.available()>1); // wait for acknowlegement
  long timerStart = millis();
  while((millis() - timerStart) < (1000UL * wait)){
    //if(mySDI12.available()) {
      //sensor can interrupt us to let us know it is done early
      //break;
    //}
  }
  //while (mySDI12.available()) {Serial.print(mySDI12.read());}
  //delay(300);                     // wait a while for a response
  mySDI12.read(); // ignore address return
  mySDI12.read(); // ignore the '+' spacer character
  // mySDI12 does not have a parseFloat function, so we'll build our own here
  float val1 = 0.0;
  bool pre_dp = true; // are we before the decimal place?
  int decimal_place = 1; // what decimal place have we reached?
  while (mySDI12.available()){
    char c = mySDI12.read();
    if (c >= '0' && c <= '9') {
      if (pre_dp == true) {
        val1 = (val1*10) + (c-'0');
      } else {
        val1 = val1 + (float(c-'0') / (10.0 * (float)decimal_place));
        decimal_place++;
      }
    } else if (c == '.') {
      pre_dp = false;
    } else {
      //end of number
      break;
    }
  }
  //mySDI12.flush();
  delay(100);
  while(mySDI12.available()) {
    mySDI12.read();
  }

  usingSDI = false;
  //return the float
  //Serial.println(val1);
  return val1;
}

ISR (SPI_STC_vect) {
  RPS.spi_stc_isr();
}
