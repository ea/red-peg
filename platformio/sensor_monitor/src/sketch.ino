#include <Wire.h>
#include <RTClib.h>
#include <MCP342x.h>
#include <SPI.h>
#include <SdFat.h>

SdFat SD;
SdFile file;
RTC_DS1307 rtc;
MCP342x adc = MCP342x(0x6F);

#define SENSOR_ACTIVE_PIN 6
#define READING_DELAY 2000
#define FILENAME_DAT "RP%06u.dat"
#define RP_SD_CS 5

uint32_t last_reading = READING_DELAY;
char g_filename[13];

void setup()
{
  // turn on the sensors
  pinMode(SENSOR_ACTIVE_PIN, OUTPUT);
  digitalWrite(SENSOR_ACTIVE_PIN, HIGH);

  Serial.begin(9600);
  Serial.println(F("starting sensor_monitor"));

  // check we have an SD card inserted
  if (!SD.begin(RP_SD_CS, SPI_HALF_SPEED)) {
    SD.initErrorHalt();
  } else {
    Serial.println(F("SD start"));
  }
  SD.mkdir("SNSR_MON");
  SD.chdir("SNSR_MON");

  // check we have an RTC attached
  if (!rtc.begin()) {
    Serial.println(F("no RTC"));
    die();
  }
  reportTimeNow();

  // check we have the expected ADC attached
  if (!adcPresent()) {
    Serial.println(F("no ADC"));
    die();
  } else {
    Serial.println(F("MCP3428 @ 0x6F"));
  }

  // pick the next file in the folder:
  uint16_t number_guess = 1U;
  bool file_exists = false;
  while (1) {
    sprintf(g_filename, FILENAME_DAT, number_guess);
    if (SD.exists(g_filename)) {
      number_guess++;
    } else {
      // and we're left with the next filename in the g_filename buffer
      break;
    }
  }
}

#define ADC1 MCP342x::channel1
#define ADC2 MCP342x::channel2
#define ADC3 MCP342x::channel3

long getAdcReading(MCP342x::Channel the_channel) {
  long adc_reading = 0;
  uint16_t timeout = 10000;
  MCP342x::Config status;
  uint8_t err = adc.convertAndRead(the_channel, MCP342x::oneShot, MCP342x::resolution16, MCP342x::gain1, timeout, adc_reading, status);
  if (err) {
    return -err;
  } else {
    // make sure the reading is positive (!)
    adc_reading = abs(adc_reading);
    return adc_reading;
  }
}

void loop()
{
  // 10 second updates
  if (millis() - last_reading >= READING_DELAY) {
    last_reading = millis();
    long adc_reading = 0;
    MCP342x::Config status;

    // first set the timestamp
    file.open(g_filename, O_RDWR | O_CREAT | O_AT_END);
    Serial.print(g_filename);
    Serial.print(F(": "));
    reportTimeNow();
    Serial.print(F(" "));
    // and send the same to the datafile
    DateTime now = rtc.now();
    char buf[5];
    sprintf(buf, "%04d", now.year());
    file.print(buf);
    file.write('/');
    sprintf(buf, "%02d", now.month());
    file.print(buf);
    file.write('/');
    sprintf(buf, "%02d", now.day());
    file.print(buf);
    file.write('-');
    sprintf(buf, "%02d", now.hour());
    file.print(buf);
    file.write(':');
    sprintf(buf, "%02d", now.minute());
    file.print(buf);
    file.write(':');
    sprintf(buf, "%02d", now.second());
    file.print(buf);
    file.print(F(" "));

    // check the TMP36 temperature (channel3)
    adc_reading = getAdcReading(ADC3);
    Serial.print(F("TMP: "));
    file.print(F("TMP: "));
    if (adc_reading < 0) {
      Serial.print(F("ERR: "));
      Serial.println(adc_reading);
      file.print(F("ERR: "));
      file.println(adc_reading);
    } else {
      // NB: the ADC connections for channel3 are reversed on Red-Peg rev.4
      //adc_reading = -adc_reading;
      float voltage = adc_reading * (2000.0 / 32000.0);
      float temperature = (voltage - 500.0) / 10.0;
      Serial.print(temperature);
      Serial.print(F("⁰C "));
      file.print(temperature);
      file.print(F("⁰C "));
    }

    // check the 4-20mA sensor (assume 0-7m level sensor) (channel1)
    adc_reading = getAdcReading(ADC1);
    Serial.print(F("LVL: "));
    file.print(F("LVL: "));
    if (adc_reading < 0) {
      Serial.print(F("ERR: "));
      Serial.println(adc_reading);
      file.print(F("ERR: "));
      file.println(adc_reading);
    } else {
      float voltage = adc_reading * (2000.0 / 32000.0);
      float water_level = (voltage - 400.0) * (7000.0 / (2000.0 - 400.0));
      if (water_level <= 0) {
        Serial.print(F("ERR_LOW "));
        file.print(F("ERR_LOW "));
      } else if (water_level >= 7000) {
        Serial.print(F("OOR_HIGH "));
        file.print(F("OOR_HIGH "));
      } else {
        Serial.print((long)water_level);
        Serial.print(F("mm "));
        file.print((long)water_level);
        file.print(F("mm "));
      }
    }

    // check the distance measurement (channel2)
    adc_reading = getAdcReading(ADC2);
    Serial.print(F("DST: "));
    file.print(F("DST: "));
    if (adc_reading < 0) {
      Serial.print(F("ERR:"));
      Serial.println(adc_reading);
      file.print(F("ERR: "));
      file.println(adc_reading);
    } else {
      // NB: the Ground supply must be connected direct, as the FET Q1 cannot cope
      // with the peak current requirement of the MaxSonar sensor. (rev.5 and below)
      float voltage = adc_reading * (5000.0 / 32000.0); // actually outputs 0–5V divided to 0-2V
      float distance = voltage / 4.9; // at Vdd=5V, MaxSonar-7060 gives ~4.9mV/cm analog reading
      if ((int)distance <= 22) {
        Serial.print(F("≤22cm "));
        file.print(F("≤22cm "));
      } else if ((int)distance >= 765) {
        Serial.print(F("≥765cm "));
        file.print(F("≥765cm "));
      } else {
        Serial.print((int)distance);
        Serial.print(F("cm "));
        file.print((int)distance);
        file.print(F("cm "));
      }
    }

    // finish the data line
    Serial.println();
    file.println();
    file.close();
  }

  if (Serial.available()) {
    // if we have serial input, check input for setting the time
    setTime();
  }
}

void setTime() {
  char c = Serial.read();
  if (c == 't' || c == 'T') {
    uint16_t year;
    uint8_t month, day, hour, min, sec;
    Serial.print(F("Year: "));
    year = Serial.parseInt();
    Serial.println(year);
    Serial.print(F("Month: "));
    month = Serial.parseInt();
    Serial.println(month);
    Serial.print(F("Day: "));
    day = Serial.parseInt();
    Serial.println(day);
    Serial.print(F("Hour: "));
    hour = Serial.parseInt();
    Serial.println(hour);
    Serial.print(F("Minute: "));
    min = Serial.parseInt();
    Serial.println(min);
    Serial.print(F("Second: "));
    sec = Serial.parseInt();
    Serial.println(sec);
    rtc.adjust(DateTime(year, month, day, hour, min, sec));
    Serial.print(F("RTC set to: "));
    reportTimeNow();
  }
}

void die() {
  pinMode(LED_BUILTIN, OUTPUT);
  while(1) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
  }
}

void reportTimeNow() {
  DateTime now = rtc.now();
  char buf[5];
  sprintf(buf, "%04d", now.year());
  Serial.print(buf);
  Serial.write('/');
  sprintf(buf, "%02d", now.month());
  Serial.print(buf);
  Serial.write('/');
  sprintf(buf, "%02d", now.day());
  Serial.print(buf);
  Serial.write('-');
  sprintf(buf, "%02d", now.hour());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.minute());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.second());
  Serial.print(buf);
  //Serial.println();
}

uint8_t adcPresent() {
  MCP342x::generalCallReset();
  delay(1); // MC342x needs 300us to settle, wait 1ms

  Wire.requestFrom(0x6F, (uint8_t)1);
  if (!Wire.available()) {
    return 0;
  } else {
    return 1;
  }
}

char* ftoa(char* a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};

 char* ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long desimal = abs((long)((f - heiltal) * p[precision]));
 itoa(desimal, a, 10);
 return ret;
}
