#include <SPI.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <HttpClient.h>
#include <Wire.h>
#include <RTClib.h>
#include <MCP342x.h>
#include <sha256.h>

enum sending_state_t {
  COUNT,
  HMAC,
  SERIAL_OUT,
  CLIENT,
  HTTP_CLIENT,
};

Sha256 Sha256;
RTC_DS1307 rtc;
MCP342x adc = MCP342x(0x6F);
EthernetClient client;
HttpClient http(client);

#define KEY "k7/mGjFMJsJHCKzMNTTQPgeIjOYcAI+tb2+TUojQFwE="
const char* k_deviceId = "355e6cff-165b-5355-80a7-38be25275cfd";
const char* k_entityId = "a3b40af1-16b5-4727-bcdc-60b0816a1e7b";
const char k_privacy[] PROGMEM = "public";
#define THE_SENDING_STRING "{\"devices\":[{\"deviceId\":\"%s\",\"entityId\":\"%s\",\"readings\":[{\"type\":\"%s\",\"unit\":\"%s\",\"period\":\"%s\"}],\"measurements\":[{\"type\":\"%s\",\"timestamp\":\"%04d-%02d-%02dT%02d:%02d:%02dZ\",\"value\":%s}]}]}"

// Name of the server we want to connect to
#define API_HOSTNAME "dev.teal-mallet.net"
// Path to api location
#define API_PATH "/api/%s/station/%s/measurements"
#define USER_AGENT_STRING "Red-Peg/1.0"

byte mac[] = { 0xDE, 0xAD, 0xFE, 0xED, 0xC0, 0xDE };

// Number of milliseconds to wait without receiving any data before we give up
const int kNetworkTimeout = 30*1000;
// Number of milliseconds to wait if no data is available before trying again
const int kNetworkDelay = 1000;

uint32_t last_send = 10000;

void setup()
{
  // turn on the sensors
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);

  Serial.begin(9600);
  Serial.println(F("starting temp_reporter"));

  Serial.println(F("Getting IP"));
  while (!Ethernet.begin(mac))
  {
    Serial.println(F("No IP"));
    delay(5000);
  }
  Serial.print("ihazip: ");
  Serial.println(Ethernet.localIP());

  if (!rtc.begin()) {
    Serial.println(F("no RTC"));
    die();
  }
  reportTimeNow();

  if (!adcPresent()) {
    Serial.println(F("no ADC"));
    die();
  } else {
    Serial.println(F("MCP3428 @ 0x6F"));
  }
}

#define ADC1 MCP342x::channel1
#define ADC2 MCP342x::channel2
#define ADC3 MCP342x::channel3

long getAdcReading(MCP342x::Channel the_channel) {
  long adc_reading = 0;
  uint16_t timeout = 10000;
  MCP342x::Config status;
  uint8_t err = adc.convertAndRead(the_channel, MCP342x::oneShot, MCP342x::resolution16, MCP342x::gain1, timeout, adc_reading, status);
  if (err) {
    return -err;
  } else {
    // make sure the reading is positive (!)
    // NB: the ADC connections for channel3 are reversed on Red-Peg rev.4
    adc_reading = abs(adc_reading);
    return adc_reading;
  }
}

void loop()
{
  // 10 second updates
  if (millis() - last_send >= 60000) {
    long adc_reading = 0;
    // check the TMP36 temperature (channel3)
    adc_reading = getAdcReading(ADC3);
    Serial.print(F("TMP: "));
    if (adc_reading < 0) {
      Serial.print(F("ERR: "));
      Serial.println(adc_reading);
    } else {
      float voltage = adc_reading * (2000.0 / 32000.0);
      float temperature = (voltage - 500.0) / 10.0;
      Serial.print(temperature);
      Serial.print(F("⁰C @ "));
      reportTimeNow();
      DateTime now = rtc.now();
      tealMalletSendData(temperature, now, "Temperature:60 sec:TMP36", "C", "INSTANT");
    }
    last_send = millis();
  }
}

int tealMalletSendData(float the_data, DateTime the_timestamp, char* type, char* unit, char* period) {
  // create a send buffer for the info
  char send_buffer[320];
  char float_buffer[10];
  ftoa(float_buffer, the_data, 2);
  sprintf(send_buffer, THE_SENDING_STRING, k_deviceId, k_entityId, type, unit, period, type, the_timestamp.year(), the_timestamp.month(), the_timestamp.day(), the_timestamp.hour(), the_timestamp.minute(), the_timestamp.second(), float_buffer);

  Serial.print(F("string length: "));
  Serial.println(strlen(send_buffer));
  Serial.println(send_buffer);

  // get the hmac of the message
  Serial.print(F("KEY length: "));
  Serial.println(strlen(KEY));
  Sha256.initHmac((uint8_t*)KEY, strlen(KEY));
  for (uint16_t i=0; i<strlen(send_buffer); i++) {
    //Serial.write(send_buffer[i]);
    Sha256.write(send_buffer[i]);
  }
  //Serial.println();
  Serial.print(F("Content-HMAC: sha256 "));
  printHash(Sha256.resultHmac(), SERIAL_OUT);
  Serial.println();
  Sha256.reset();
  for (uint16_t i=0; i<strlen(send_buffer); i++) {
    Sha256.write(send_buffer[i]);
  }

  // send the data
  int err=0;
  http.beginRequest();
  char buff[150];
  sprintf(buff, API_PATH, k_entityId, k_deviceId);
  Serial.print(F("sending to: "));
  //Serial.print(API_HOSTNAME);
  Serial.print(API_HOSTNAME);
  Serial.println(buff);
  err = http.post(API_HOSTNAME, buff, USER_AGENT_STRING);
  if (err == 0){
    Serial.println(F("requesting"));
    http.sendHeader("Content-Type", "application/json");
    client.print("Content-HMAC: sha256 "); printHash(Sha256.resultHmac(), CLIENT); client.println();
    http.sendHeader("Content-Length", strlen(send_buffer));
    http.println(send_buffer);
    http.endRequest();

    err = http.responseStatusCode();
    if ((err < 200 || err > 299 )) {
      // there was a problem
      Serial.print(F("Borked: "));
      Serial.println(err);
      err = http.skipResponseHeaders();
      if (err >= 0) {
        int bodyLen = http.contentLength();
        Serial.print(F("Content length is: "));
        Serial.println(bodyLen);
        Serial.println();
        Serial.println(F("Body returned follows:"));

        // Now we've got to the body, so we can print it out
        unsigned long timeoutStart = millis();
        char c;
        // Whilst we haven't timed out & haven't reached the end of the body
        while ( ( http.connected() || http.available() ) && ( millis() - timeoutStart ) < kNetworkTimeout ) {
          if (http.available()) {
            c = http.read();
            // Print out this character
            Serial.print(c);

            bodyLen--;
            // We read something, reset the timeout counter
            timeoutStart = millis();
          } else {
            // We haven't got any data, so let's pause to allow some to
            // arrive
            delay(kNetworkDelay);
          }
        }
      }
    } else {
      Serial.print(F("Went OK: "));
      Serial.println(err);
    }
    http.flush();
    http.stop();
  } else {
    Serial.print(F("no connection err: "));
    Serial.println(err);
  }
  return err;
}

void die() {
  pinMode(LED_BUILTIN, OUTPUT);
  while(1) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
  }
}

void reportTimeNow() {
  DateTime now = rtc.now();
  char buf[5];
  sprintf(buf, "%04d", now.year());
  Serial.print(buf);
  Serial.write('/');
  sprintf(buf, "%02d", now.month());
  Serial.print(buf);
  Serial.write('/');
  sprintf(buf, "%02d", now.day());
  Serial.print(buf);
  Serial.write('-');
  sprintf(buf, "%02d", now.hour());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.minute());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.second());
  Serial.print(buf);
  Serial.println();
}

uint8_t adcPresent() {
  MCP342x::generalCallReset();
  delay(1); // MC342x needs 300us to settle, wait 1ms

  Wire.requestFrom(0x6F, (uint8_t)1);
  if (!Wire.available()) {
    return 0;
  } else {
    return 1;
  }
}

char* ftoa(char* a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};

 char* ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long desimal = abs((long)((f - heiltal) * p[precision]));
 itoa(desimal, a, 10);
 return ret;
}

int printHash(uint8_t* hash, sending_state_t sending_type) {
  int i;
  Print* s;
  if (sending_type == SERIAL_OUT) {
    s = &Serial;
  } else if (sending_type == CLIENT) {
    s = &client;
  } else if (sending_type == HTTP_CLIENT) {
    s = &http;
  } else {
    return -1;
  }
  for (i=0; i<32; i++) {
    s->print("0123456789abcdef"[hash[i]>>4]);
    s->print("0123456789abcdef"[hash[i]&0xf]);
  }
  //s->.println();
  return 0;

}
