#include <SPI.h>
#include <SdFat.h>
#include <Ethernet.h>
#include <HttpClient.h>
#include <EthernetClient.h>
#include <Wire.h>
#include "RTClib.h"
#include <MCP342x.h>

SdFat SD;
SdFile file;
RTC_DS1307 rtc;
// 0x6F is the configured address for MCP3428 on the red-peg shield
uint8_t address = 0x6F;
MCP342x adc = MCP342x(address);

#define FILENAME_DAT "RP%06u.dat"

// choose the data format (AMON/XML/CSV), default is JSON
#define FORMAT_AMON
//#define FORMAT_XML
//#define FORMAT_CSV

#define BUFFER_SIZE 10

//#define REPORTING_PERIOD 1000*60*60*12 // 12hours
#define REPORTING_PERIOD 1000UL*60UL*5UL // 5 minutes
//#define REPORTING_PERIOD 1000UL*60UL // 1 minutes
//#define RECORDING_PERIOD 1000*16*15 // 15 minutes
#define RECORDING_PERIOD 1000UL*60UL // 1 minute
//#define RECORDING_PERIOD 1000UL*10UL // 10 seconds

const int k_chipSelect = 4;
const char* k_deviceId = "c1d921b0-4060-5dc2-9ff9-97872a9f3d69";
const char* k_entityId = "a3b40af1-16b5-4727-bcdc-60b0816a1e7b";
const char k_privacy[] PROGMEM = "public";

typedef struct {
  char type[30]; // reading:type
  char unit[5]; // reading:unit
  char period[8]; // reading:period
} t_SensorType;

const t_SensorType theSensor[1] PROGMEM = {"Temperature:1 min:TMP36", "C", "INSTANT"};

typedef struct {
  uint8_t sensor; // id of the sensor from SensorType array
  uint32_t timestamp; // the millis() record of the event
  float data; // the actual stored data
} t_SensorData;

t_SensorData theData[BUFFER_SIZE];

uint8_t p_head = 0;
uint8_t p_tail = 0;

uint32_t g_last_report = 0UL;
uint32_t g_last_record = -RECORDING_PERIOD;

char g_filename[13] = "";
uint16_t g_currentFileNumber = 1;
uint16_t g_nextFileToSend = 0;
uint16_t g_lastFileSent = 0;

// Error messages stored in flash.
#define error(msg) sd.errorHalt(F(msg))

// Name of the server we want to connect to
#define API_HOSTNAME "dev.teal-mallet.net"
// Path to api location
#define API_PATH "/api/%s/station/%s/measurements"
#define USER_AGENT_STRING "Red-Peg/1.0"

byte mac[] = { 0xDE, 0xAD, 0xFE, 0xED, 0xC0, 0xDE };

// Number of milliseconds to wait without receiving any data before we give up
const int kNetworkTimeout = 30*1000;
// Number of milliseconds to wait if no data is available before trying again
const int kNetworkDelay = 1000;

void setup()
{
  Serial.begin(9600);
  pinMode(A1, OUTPUT);
  digitalWrite(A1, HIGH);
  pinMode(A2, INPUT);
  digitalWrite(A2, LOW);
  pinMode(A3, OUTPUT);
  digitalWrite(A3, LOW);

  if (!SD.begin(k_chipSelect, SPI_HALF_SPEED)) {
    SD.initErrorHalt();
  } else {
    Serial.println(F("SD start"));
  }

  if (! rtc.begin()) {
    Serial.println(F("no RTC"));
    while (1);
  }

  // optional: set the rtc with a known time/date
  //rtc.adjust(DateTime(2015, 12, 15, 17, 23, 0));

  // test the RTC and see what we have
  DateTime now = rtc.now();
  char buf[5];
  sprintf(buf, "%04d", now.year());
  Serial.print(buf);
  Serial.write('/');
  sprintf(buf, "%02d", now.month());
  Serial.print(buf);
  Serial.write('/');
  sprintf(buf, "%02d", now.day());
  Serial.print(buf);
  Serial.write('-');
  sprintf(buf, "%02d", now.hour());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.minute());
  Serial.print(buf);
  Serial.write(':');
  sprintf(buf, "%02d", now.second());
  Serial.print(buf);
  Serial.println();

  MCP342x::generalCallReset();
  delay(1); // MC342x needs 300us to settle, wait 1ms

  // Check device present
  Wire.requestFrom(address, (uint8_t)1);
  if (!Wire.available()) {
    Serial.print("No device found at address ");
    Serial.println(address, HEX);
    while (1)
      ;
  }

  while (Ethernet.begin(mac) != 1)
  {
    Serial.println("No IP");
    delay(15000);
  }
  Serial.print("ihazip: ");
  Serial.println(Ethernet.localIP());

  // change to sub-directory to avoid 512 file limit in the root dir (FAT16)
  SD.mkdir("RED-PEG");
  SD.chdir("RED-PEG");

  // check the existing files and build the next filename
  g_currentFileNumber = lastFileUsed();
  g_currentFileNumber = nextFile(g_currentFileNumber);
  // initialise the last sent file as the one before the next
  g_lastFileSent = g_currentFileNumber -1;
  g_nextFileToSend = g_lastFileSent;
  Serial.print(F("next file: "));
  Serial.println(g_filename);
}

void closeFile(uint16_t fileNumber, boolean write_footers = false)
{
  if (write_footers) {
    // prep the filename pointers to send that file
    g_nextFileToSend = g_currentFileNumber;
    g_currentFileNumber = nextFile(g_currentFileNumber);
  }
  file.close();
  Serial.print(g_filename);
  Serial.println(F(" closed."));
}

void loop()
{
  // check if there's data to save to the SD card
  writeData();

  // check if we've reached the end of the reporting period
  if (millis() - g_last_report >= REPORTING_PERIOD) {
    Serial.println(F("clzfile"));
    //close the file and write the footers
    closeFile(g_currentFileNumber, true);
    g_last_report = millis();
    openFile(g_currentFileNumber);
  }

  // check if we've reached the next time to record a reading
  if (millis() - g_last_record >= RECORDING_PERIOD) {
    /*
    //getting the voltage reading from the temperature sensor
    int reading = analogRead(A2);
    // converting that reading to voltage, for 3.3v arduino use 3.3
    float voltage = reading * 5.0;
    voltage /= 1024.0;
    // print out the voltage
    Serial.print(voltage); Serial.println("V");
    // now print out the temperature
    float temperatureC = (voltage - 0.5) * 100.0 - 10.0;  //converting from 10 mv per degree wit 500 mV offset
                                                 //to degrees ((voltage - 500mV) times 100)
    */

    long value = 0;
    float temperatureC = 0.0;
    MCP342x::Config status;
    // Initiate a conversion; convertAndRead() will wait until it can be read
    uint8_t err = adc.convertAndRead(MCP342x::channel2, MCP342x::oneShot,
             MCP342x::resolution16, MCP342x::gain1,
             1000000, value, status);
    if (err) {
      Serial.print("Convert error: ");
      Serial.println(err);
    } else {
      // convert the 0-32786 reading into 0-2.048V, then -0.5V for zero and change to °C
      temperatureC = float(value)/160.0 - 50;
    }

    // increment the buffer head and record the data
    recordSensor(0, temperatureC);
    g_last_record = millis();

    Serial.print(temperatureC); Serial.println("°C");
  }

  if (g_lastFileSent != g_nextFileToSend) {
    // send any outstanding files
    // we'll assume that everything between the number of the last file
    // and the current is to go. This might give duplicate sends if there's
    // some existing datafile numbers though
      int err=0;
      EthernetClient c;
      HttpClient http(c);
      http.beginRequest();
      char buff[150];
      sprintf(buff, API_PATH, k_entityId, k_deviceId);
      Serial.print(F("sending to: "));
      Serial.print(API_HOSTNAME);
      Serial.println(buff);
      err = http.post(API_HOSTNAME, buff, USER_AGENT_STRING);
      if (err == 0){
        Serial.println(F("requesting"));
#ifdef FORMAT_XML
        http.sendHeader("Content-Type", "application/xml");
#else
        http.sendHeader("Content-Type", "application/json");
#endif
        char filename[13] = "";
        sprintf(filename, FILENAME_DAT, g_nextFileToSend);
        file.close();
        file.open(filename);
        file.seekEnd(0);
        Serial.print(F("sending "));
        Serial.println(filename);
        uint32_t file_length = file.curPosition();
        http.sendHeader("Content-Length", file_length);
        // Serial.print(F("Content-Length: "));
        // Serial.println(file_length);
        // set the file pointer to the beginning of the file
        file.seekSet(0);
        char c = 0;
        while ((c = file.read()) > 0) {
          //Serial.write(c);
          http.write(c);
        }
        http.endRequest();

        err = http.responseStatusCode();
        if ((err < 200 || err > 299 )) {
          // there was a problem
          Serial.print(F("Borked: "));
          Serial.println(err);
          err = http.skipResponseHeaders();
          if (err >= 0) {
            int bodyLen = http.contentLength();
            Serial.print("Content length is: ");
            Serial.println(bodyLen);
            Serial.println();
            Serial.println("Body returned follows:");

            // Now we've got to the body, so we can print it out
            unsigned long timeoutStart = millis();
            char c;
            // Whilst we haven't timed out & haven't reached the end of the body
            while ( (http.connected() || http.available()) &&
                   ((millis() - timeoutStart) < kNetworkTimeout) )
            {
                if (http.available())
                {
                    c = http.read();
                    // Print out this character
                    Serial.print(c);

                    bodyLen--;
                    // We read something, reset the timeout counter
                    timeoutStart = millis();
                }
                else
                {
                    // We haven't got any data, so let's pause to allow some to
                    // arrive
                    delay(kNetworkDelay);
                }
            }
          }
        } else {
          Serial.print(F("Went OK: "));
          Serial.println(err);
        }
        http.flush();
        http.stop();
        // increment the file send counters
        g_lastFileSent = g_nextFileToSend;
      }
      file.sync();
      file.close();
      delay(10);
      openFile(g_currentFileNumber);
  }
}

void recordSensor(uint8_t sensor, float data)
{
  p_head = (p_head +1) % BUFFER_SIZE;
  theData[p_head].sensor = sensor;
  theData[p_head].timestamp = millis();
  theData[p_head].data = data;
}

uint16_t lastFileUsed() {
  // make a binary search to find where we're up to quicker
  // than the standard linear search for >500~ files
  uint16_t numberGuess = 1U;
  uint16_t nextJump = 32768U; // half the max 16bit max file number
  char k_filename[13];
  while (nextJump > 1) {
    sprintf(k_filename, FILENAME_DAT, numberGuess);
    if (SD.exists(k_filename)) {
      Serial.print(k_filename);
      Serial.println(F(" exists"));
      // check the next file
      sprintf(k_filename, FILENAME_DAT, numberGuess+1);
      if (SD.exists(k_filename)) {
        // we're not at the end yet
        numberGuess = numberGuess + nextJump;
        nextJump = nextJump / 2;
      } else {
        // this number exists, but the next doesn't
        // this must the the last file in a continuous list
        return numberGuess+1;
      }
    } else {
      // it doesn't exist, guess lower
      Serial.print(k_filename);
      Serial.println(F(" doesn't"));
      numberGuess = numberGuess - nextJump;
      nextJump = nextJump / 2;
    }
  }
  // or we happen to have got the exact number
  return numberGuess;
}

uint16_t nextFile(uint16_t fileNumber)
{
  // build the filename:
  // this will die once there's 2^16 files on the card.
  // but that nicely matches the max number of files on FAT16
  while (1) {
    sprintf(g_filename, FILENAME_DAT, fileNumber);
    if (SD.exists(g_filename)) {
      Serial.print(g_filename);
      Serial.println(" exists");
      fileNumber++;
    } else {
      break;
    }
  }
  return fileNumber;
}

void openFile(uint16_t fileNumber)
{
  bool new_file = false;
  sprintf(g_filename, FILENAME_DAT, fileNumber);
  // check if the file exists
  if (!SD.exists(g_filename)) {
    // if not, flag for writing the intial metadata
    new_file = true;
  }
  if (!file.open(g_filename, O_RDWR | O_CREAT | O_AT_END)) {
    Serial.print(F("Error opening "));
    Serial.print(g_filename);
  }
  if (new_file == true) {
    Serial.print(F("New file "));
    Serial.print(g_filename);
    Serial.println(F(" opened"));
    Serial.println(F("Writing headers..."));
    // write the metadata for starting a new file
    writeHeaders();
  }
  file.sync();
  // leave the file open for the next function
}

void writeData()
{
  // writes out a line of data to the currentFile if available
  if (p_tail != p_head ) {
    // send a line of data to the file if it's there's some in the buffer
    // open the file for writing
    openFile(g_currentFileNumber);
    // then rewind the data back past the known footer position
    stripFooters();
    // write out each line
    while (p_tail != p_head) {
      p_tail = (p_tail +1) % BUFFER_SIZE;
      // writes a line of data
      file.print(F("{\"type\": \""));
      char buf[30];
      strcpy_P(buf, theSensor[theData[p_tail].sensor].type);
      file.print(buf);
      file.print(F("\", \"timestamp\": \""));
      // Set the time from the RTC: yyyy-mm-ddThh:mm:ss-hhZ
      DateTime record = rtc.now();
      // adjust the time from the rtc minus how long ago we recorded the data
      record = record.unixtime() + (millis() - theData[p_tail].timestamp);
      sprintf(buf, "%04d", record.year());
      file.write(buf);
      file.write('-');
      sprintf(buf, "%02d", record.month());
      file.print(buf);
      file.write('-');
      sprintf(buf, "%02d", record.day());
      file.print(buf);
      file.write('T');
      sprintf(buf, "%02d", record.hour());
      file.print(buf);
      file.write(':');
      sprintf(buf, "%02d", record.minute());
      file.print(buf);
      file.write(':');
      sprintf(buf, "%02d", record.second());
      file.print(buf);
      file.print("Z"); // assume UTC time
      file.print(F("\", \"value\": "));
      file.print(theData[p_tail].data);
      file.println(F("},"));
    }
    // then write the footers
    writeFooters();
    file.sync();
    file.close();
  }
}

void writeHeaders()
{
  DateTime now = rtc.now();
  file.timestamp(T_CREATE | T_WRITE, now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second());
#ifdef FORMAT_XML
  //writeXMLHeaders();
  file.println(F("XML Headers are not yet supported"))
#elif FORMAT_CSV
  //writeCSVHeaders();
  file.println(F("CSV Headers are not yet supported"))
#else // default to FORMAT_AMON
  char buf[45];
  file.println(F("{"));
  file.println(F("\"devices\": ["));
  file.println(F("{"));
  file.print(F("\"deviceId\": \""));
    //strcpy_P(buf, k_deviceId);
    //file.print(buf);
    file.print(k_deviceId);
      file.println(F("\","));
  file.print(F("\"entityId\": \""));
    //strcpy_P(buf, k_entityId);
    //file.print(buf);
    file.print(k_entityId);
      file.println(F("\","));
  file.print(F("\"privacy\": \""));
    strcpy_P(buf, k_privacy);
    file.print(buf);
      file.println(F("\","));
  file.println(F("\"readings\": ["));
  file.println(F("{"));
  file.print(F("\"type\": \""));
    strcpy_P(buf, theSensor[0].type);
    file.print(buf);
      file.println(F("\","));
  file.print(F("\"unit\": \""));
    strcpy_P(buf, theSensor[0].unit);
    file.print(buf);
      file.println(F("\","));
  file.print(F("\"period\": \""));
    strcpy_P(buf, theSensor[0].period);
    file.print(buf);
      file.println(F("\""));
  file.println(F("}"));
  file.println(F("],"));
  file.println(F("\"measurements\": ["));
#endif
  writeFooters();
}

void writeFooters()
{
#ifdef FORMAT_XML
  //writeXMLFooters();
  file.println(F("XML Footers are not yet supported"))
#elif FORMAT_CSV
  //writeCSVFooters();
  file.println(F("CSV Footers are not yet supported"))
#else // default to FORMAT_AMON
  // rewind past the newline and overwrite the last comma
  file.seekCur(-3);
  file.println(F("\n]\n}\n]\n}"));
#endif
}

void stripFooters()
{
#ifdef FORMAT_XML
  // remove the XML footers
#elif FORMAT_CSV
  // csv has no footer
#else
  // default to FORMAT_AMON
  file.seekCur(-11);
  //Serial.print(F("file char: "));
  //Serial.println(file.peek());
  if (file.peek() == '}') {
    // we're at the end of the previous record
    file.println(F("},"));
  } else {
    // we're at the first record
    file.println(F(" ["));
  }
  #endif
}

void dateTime(uint16_t* date, uint16_t* time) {
   uint16_t year;
   uint8_t month, day, hour, minute, second;

   // User gets date and time from GPS or real-time clock here
   DateTime now = rtc.now();
   year = now.year();
   month = now.month();
   day = now.day();
   hour = now.hour();
   minute = now.minute();
   second = now.second();

   // return date using FAT_DATE macro to format fields
   *date = FAT_DATE(year, month, day);

   // return time using FAT_TIME macro to format fields
   *time = FAT_TIME(hour, minute, second);
 }
